<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iDream Tech</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>Products</h1>
                        <p>Our Work Showcase </p>
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li class="active"><a href="javascript:void(0)">Products</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">

        <!-- container -->
        <div class="container">
            <!-- product row -->
            <div class="row product-list-item">
                <!-- col -->
                <div class="col-lg-6 col-sm-6 aos-item" data-aos="fade-up" >
                    <img src="img/product01.jpg" alt="" class="img-fluid">
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 col-sm-6 align-self-center aos-item" data-aos="fade-down">
                    <div class="p-md-3 p-2">
                        <article>
                            <h3 class="h3">Co-operative society Management System</h3>
                            <p>iDream Tech offers you hassle-free platform to transform your society, making it highly functional and easily manageable. Developed by an expert team with over 25year s of experience handling society matters,</p>
                            <a href="cooperative-society-management.php" class="idlink">
                                Read More <span class="icon-arrow-right icomoon"></span>
                            </a>
                        </article>    
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!-- product row -->

             <!-- product row -->
             <div class="row product-list-item">
                <!-- col -->
                <div class="col-lg-6 col-sm-6 order-lg-last order-sm-last aos-item" data-aos="fade-up">
                    <img src="img/product02.jpg" alt="" class="img-fluid">
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 col-sm-6 align-self-center aos-item" data-aos="fade-down">
                    <div class="p-md-3 p-2">
                        <article>
                            <h3 class="h3">Integrated E-Pay Services</h3>
                            <p>Using our web service, you can manage all your payments outside of ePay's system. This means you can manage payments from your online shop without logging in to ePay's administration.</p>
                            <a href="integrated-epayservices.php" class="idlink">
                                Read More <span class="icon-arrow-right icomoon"></span>
                            </a>
                        </article>    
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!-- product row -->

            <!-- product row -->
            <div class="row product-list-item">
                <!-- col -->
                <div class="col-lg-6 col-sm-6 aos-item" data-aos="fade-up">
                    <img src="img/product03.jpg" alt="" class="img-fluid">
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 col-sm-6 align-self-center aos-item" data-aos="fade-down">
                    <div class="p-md-3 p-2">
                        <article>
                            <h3 class="h3">Smart and Virtual Classes</h3>
                            <p>Using our web service, you can manage all your payments outside of ePay's system. This means you can manage payments from your online shop without logging in to ePay's administration.</p>
                            <a href="smart-virtual-classes.php" class="idlink">
                                Read More <span class="icon-arrow-right icomoon"></span>
                            </a>
                        </article>    
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!-- product row -->            

             <!-- product row -->
             <div class="row product-list-item">
                <!-- col -->
                <div class="col-lg-6 col-sm-6 aos-item order-lg-last order-sm-last" data-aos="fade-up">
                    <img src="img/product05.jpg" alt="" class="img-fluid">
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 col-sm-6 align-self-center aos-item" data-aos="fade-down">
                    <div class="p-md-3 p-2">
                        <article>
                            <h3 class="h3">HRMS Solutions</h3>
                            <p>Comprehensive HR software (often referred to as HRMS, HRIS or HCM software) covers many modules and has broad functionality. </p>
                            <a href="hrms-solutions.php" class="idlink">
                                Read More <span class="icon-arrow-right icomoon"></span>
                            </a>
                        </article>    
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!-- product row -->
            
             <!-- product row -->
             <div class="row product-list-item">
                <!-- col -->
                <div class="col-lg-6 col-sm-6  aos-item" data-aos="fade-up">
                    <img src="img/product06.jpg" alt="" class="img-fluid">
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 col-sm-6 align-self-center aos-item" data-aos="fade-down">
                    <div class="p-md-3 p-2">
                        <article>
                            <h3 class="h3">School Management</h3>
                            <p>Our Education Management System is unique solution developed for educational institutes to manage the functions like Content Management System, Student Management System,</p>
                            <a href="school-management.php" class="idlink">
                                Read More <span class="icon-arrow-right icomoon"></span>
                            </a>
                        </article>    
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!-- product row -->
            
             <!-- product row -->
             <div class="row product-list-item">
                <!-- col -->
                <div class="col-lg-6 col-sm-6 aos-item order-lg-last order-sm-last" data-aos="fade-up">
                    <img src="img/product07.jpg" alt="" class="img-fluid">
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 col-sm-6 align-self-center aos-item" data-aos="fade-down">
                    <div class="p-md-3 p-2">
                        <article>
                            <h3 class="h3">Health Care Automation</h3>
                            <p>aazeero P2P automation is the patented and the only automated AI, IoT, and Cloud-based technology for Procurement to Pricing Automation in healthcare on the planet. </p>
                            <a href="healthcare-automation.php" class="idlink">
                                Read More <span class="icon-arrow-right icomoon"></span>
                            </a>
                        </article>    
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!-- product row -->
        </div>
        <!--/ container -->  

        <!-- white box -->
        <div class="whitebox py20">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 col-sm-6 align-self-center aos-item" data-aos="fade-down">
                        <h3 class="h3">Why Thousands Of Clients Choose</h3>
                        <p class="pb-3">we build results-oriented brand strategies and continually refine your canpaigns for the greatest outcome. from full-scale digital marketing and advertising strategy, right through to our precise execution and reporting that’s right,we’ve got you covered</p>
                        <p>
                            <a href="contact.php" class="idlink">
                                Contact us <span class="icon-arrow-right icomoon"></span>
                            </a>
                        </p>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-6 col-sm-6 aos-item" data-aos="fade-up">
                        <img src="img/client-choose.png" alt="" class="img-fluid">
                     </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ whitebox -->
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->
    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>