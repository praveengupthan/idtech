<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>School Management</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>School Management</h1>                        
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="products-all.php">Products</a></li>
                            <li class="active"><a href="javascript:void(0)">School Management</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-2">
                    <!-- col -->
                    <div class="col-lg-12 col-sm-12 align-self-center aos-item" data-aos="fade-up">                        
                        <h3 class="h4 py-2 fbold">Educational Management System</h3>
                        <p>Our Education Management System is unique solution developed for educational institutes to manage the functions like Content Management System, Student Management System, Online Examination, Library, Video Conferencing, Virtual Classroom, clickers, Kiosk etc. It includes processing, analyzing, and reporting of educational information including teacher, students, staff, and payroll library management etc.</p>  

                        <h3 class="h4 py-2 fbold">EMS Services</h3>
                        <p><span class="fbold">Institutes Management System </span> – Brilliant offer complete Cloud based Solution for institutes which Student Enrollment, Multi Branch Operation, Course Configuration, Scheduling, Petty Cash, and Library, fee Collection, Teacher Payroll and Taxation. Ideal solution for Institutes, school and colleges. </p>

                        <p><span class="fbold">Learning Management System </span> – Brilliant Offer Learning Management System where you can configure your own e-learning course with videos, Online Test, Flash cards, Analytical Reports, Student enrollment, Online Payments, Payment through Coupons, Teacher student communication through Skype, Partner and franchise management. </p>
                    </div>
                    <!--/ col -->                  

                    <!-- col -->
                    <div class="col-lg-10 mx-auto">
                        <img src="img/school-management2.png" alt="" class="img-fluid w-100 aos-item" data-aos="fade-down">
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row py-2">
                    <!-- col -->
                    <div class="col-lg-6 align-self-center order-lg-last aos-item" data-aos="fade-up">
                        <img src="img/graphics/school-management.svg" class="img-fluid w-100" alt="">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-down">     
                        <h3 class="h4 py-2 fbold">Residential School Modules</h3>
                        <ul class="list-items">
                        <li>Student Requisition</li>
                        <li>Parent’s Portal / Communication</li>
                        <li>Infirmary</li>
                        <li>Student Life Cycle</li>
                        <li>Mess / Store Management</li>
                        <li>Fee / Bill Management</li>
                        <li>Travel Desk</li>
                        </ul> 

                        <h3 class="h4 py-2 fbold">Financial & Admin Processes</h3>
                        <ul class="list-items">
                            <li>Financial Accounting</li>
                            <li>Leave Management</li>
                            <li>Asset &amp; Procurement Management</li>
                            <li>Payroll Management</li>
                            <li>E-Library</li>
                            <li>Fee / Bill Management</li>
                            <li>Travel Desk</li>
                        </ul> 

                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-12 text-center">
                        <h3 class="h2 py-2 fbold">Benefits</h3>
                    </div>
                    <!-- /col -->
                    <!-- col -->
                    <div class="col-lg-4 aos-item" data-aos="fade-up">
                        <h3 class="h4 py-2 fbold">Parents</h3>
                        <ul class="list-items">
                            <li>Real time academic performance</li>
                            <li>Track with past performance. All  report cards at one place</li>
                            <li>Requests to school, teachers through help desk</li>
                            <li>Travel requests online</li>
                            <li>Real time fee status</li>
                            <li>Online fee payments</li>
                            <li>Real time view of what books the child is reading</li>
                            <li>Real time health monitoring of the child</li>
                            <li>Real time view of what the child is consuming / purchasing</li>
                        </ul>
                    </div>
                    <!-- /col -->

                     <!-- col -->
                     <div class="col-lg-4 aos-item" data-aos="fade-down">
                        <h3 class="h4 py-2 fbold">Management / Principal</h3>
                        <ul class="list-items">
                            <li>Real time tracking of fees and costs</li>
                            <li>Fee bill preparation – reduction in cycle time, costs and errors</li>
                            <li>Exam grading – result publication reduction in  cycle time, costs and errors</li>
                            <li>Effective Pending fee follow-ups</li>
                            <li>Better co-ordination with teachers / staff (e-Notice)</li>
                            <li>Tracking and facilitating  unresolved grievances (help desk)</li>
                        </ul>
                    </div>
                    <!-- /col -->

                     <!-- col -->
                     <div class="col-lg-4 aos-item" data-aos="fade-up">
                        <h3 class="h4 py-2 fbold">Faculty</h3>
                        <ul class="list-items">
                            <li>Reduced time requirements for administrative activities</li>
                            <li>Can focus on data based managements of</li>
                        </ul>
                    </div>
                    <!-- /col -->
                </div>
                <!--/ row -->                
            </div>
            <!--/ container --> 
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->
    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>