<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ITES</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>ITES</h1>                        
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="products-all.php">Services</a></li>
                            <li class="active"><a href="javascript:void(0)">ITES</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
             <!-- product row -->
                 <div class="row product-list-item py-sm-2">
                    <!-- col -->
                    <div class="col-lg-6 aos-item" data-aos="fade-up" >
                        <p>At iDream Tech we are passionate about deliveries related to IT enabled services (ITeS). We would love to turn your challenges into an achievable business opportunity, which can impetus your business with improved efficiency and self-sustainability.</p>

                        <p>Whether it’s engineering or re-engineering – we provide a  SLA driven and affordable solutions, coupled with continuous support and training at each phase as our USP. Our team can efficiently work with you to bring about the desired outputs in Web solutions, Design solutions or Hosting solutions that suits your needs. </p>

                        <article> 
                            <p><span class="fbold">We have expertise and experience in running outsourced </span> processes that can be enabled with information technology and covers diverse areas like finance, HR, administration, health care, telecommunication, manufacturing etc. Armed with technology and manpower, we can operate from any locations across globe and India being still a most desired destination for any out sourcing business. This would reduce transactional and overhead costs and improve service standards. </p>
                        </article>                        
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 aos-item" data-aos="fade-down">
                        <img src="img/graphics/ites.svg" class="img-fluid w-100">
                    </div>
                    <!--/ col -->
                </div>
                <!-- product row -->               

            <!-- steps -->
            <div class="row py-md-3">
                <!-- col -->
                 <div class="col-lg-6 aos-item" data-aos="fade-down">
                    <img src="img/graphics/coe.svg" alt="" class="img-fluid">
                </div>
                <!--/ col -->      
                <!-- col -->
                <div class="col-lg-6 align-self-center">
                <h3 class="h4 fbold">Center of Excellenec (CoE)</h3>
                    <p>IDream has an expertise in setting-up Center of Excellenec (CoE) for MNCs and domestic companies. We are having Shared Services and Transaction Process expertise of more than 200 collective years. All our services are based on SMEs,KPOs,etc.and other relevant advisory services.</p>

                    <p class="fbold">Few of our Offerings:</p>
                    <ul class="list-items">
                        <li>Call Center- Domestic and International</li>
                        <li>Legal Process Outsourcing (LPO_/KPO(Knowledge Process Outsourcing)</li>
                        <li>Business Process Management (BPM)</li>
                        <li>Data Entry Operations</li>
                        <li>Shared Secvices- Transaction Process</li>
                        <li>Medical Transcriptions</li>
                    </ul>
                </div>
                <!--/col -->                        
            </div>
            <!--/ steps -->              
            </div>
            <!--/container -->
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->
    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>