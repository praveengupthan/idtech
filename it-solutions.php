<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>IT Solutions</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>IT Solutions</h1>                        
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="#">Services</a></li>
                            <li class="active"><a href="javascript:void(0)">IT Solutions</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
               <!-- product row -->
               <div class="row product-list-item py-sm-3">
                <!-- col -->
                <div class="col-lg-6 aos-item align-self-center" data-aos="fade-up" >
                <h5 class="h5 fbold">“You Might have a complex IT requirement, we try to simplify it for making life Easy”</h5>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 align-self-center aos-item" data-aos="fade-down">
                    <div class="p-md-3 p-2">
                        <article>                           
                            <p>Our customized IT solutions can assist you to reduce operational costs, extend capabilities and sustain the profitability through integrating automation in your business processes.</p>

                            <p>Multi-vendor IT support services can help you simplify IT support management by streamlining multiple OEM and vendor contracts to an One-Stop Solution Provider with an expertise to care of all your technology support needs to improve availability, resolve issues quickly and reduce outages with our proactive, reactive support across your IT environment.</p>
                        </article>    
                    </div>
                </div>
                <!--/ col -->
                </div>
                <!-- product row -->
                <!-- tab -->
                <div class="tab_wrapper second_tab custom-tab left-tablist">
                    <ul class="tab_list aos-item" data-aos="fade-up">
                        <li class="active">Application Development</li>                        
                        <li>Mobile App Development</li>
                        <li>Web Development</li>
                        <li>AI, ML and AR</li>
                        <li>BI Solutions</li>                       
                    </ul>

                    <div class="content_wrapper aos-item" data-aos="fade-down">

                        <!-- application development -->
                        <div class="tab_content active">
                            <h3 class="h4 fbold tabtitle">Application Development</h3>
                            <img src="img/application-development.jpg" alt="" class="img-fluid">
                            <p class="pt-3">We provide application support and maintenance services for enterprises on windows and linux platforms. </p>
                            <p>SI’s maintenance and support team are available round the clock onsite, offsite and offshore to support your application. Our maintenance capabilities include:</p>

                            <ul class="list-items">
                                <li>Documentation of existing applications</li>
                                <li>Production Support</li>
                                <li>Incident Report (IR) resolution process clear off outstanding IRs</li>
                                <li>Root-cause analysis</li>
                                <li>Enhancements</li>
                                <li>Testing</li>
                                <li>Release Management</li>
                            </ul>

                            <p>24x7 Support & Maintenance of application systems</p>
                            <h3 class="h5 fbold">Technology Focus</h3>
                            <p>Currently our team focusing on supporting applications developed using the following technologies.</p>

                            <ul class="list-items">
                                <li>Microsoft SharePoint</li>
                                <li>Microsoft .NET</li>
                                <li>Java, J2EE</li>
                                <li>PHP, Drupal, Joomla, Wordpress and other opensource technologies</li>
                                <li>Applications hosted on Windows Azure and Amazon ES2</li>                                
                            </ul>
                        </div>
                        <!-- /application development -->

                        <!-- mobile app development -->
                        <div class="tab_content">
                            <h3 class="h4 fbold tabtitle">Mobile Development</h3>
                            <img src="img/mobile-app-development.jpg" alt="" class="img-fluid">
                            <p class="pt-3">Discover Mobility with an Innovative, Scalable and Cost-Effective Business Approach
                            As the leading mobile app development company, iDream Tech has extensive experience in creating enterprise-grade, native & hybrid mobile applications that power mission-critical processes and support your digital transformation journey. It is along with mobile enablement and re-engineering services across all the major mobile platforms including iOS, Android, and Windows Mobile.</p>

                            <p>iDream Tech offers insightful expertise in mobile app development by building your cross-platform mobile applications that seamlessly work across devices or platforms. As a custom mobile application development company, we provide secure, scalable and sustainable solutions to our valued clients ranging from Startups, Small and Medium to Large Enterprises.</p>

                            <img src="img/graphics/mobile-app-development.svg" alt="" class="img-fluid w-100">

                            <p>Mobile App- We in iDream Tech work on Mobile Applications that can run on mobile devices and a typical Mobile Application utilizes a network connection to work with remote computing sources. It involves creating bundles of software -</p>
                            <p>codes, binaries, assets, Inventory, etc., implementing back-end services such as data access with multiple APIs, and testing on the targeted devices.</p>
                            <p>Mobile applications are intuitive and accessible. Mobile apps can be leveraged to create a multi-channel / omni-channel customer experience.</p>
                            <p>We have a team of highly skilled Andriod and iOS development professionals and we follow standard industry practices.</p>

                        </div>
                        <!-- /mobile app development -->

                          <!-- Web development -->
                          <div class="tab_content">
                            <h3 class="h4 fbold tabtitle">Web Development</h3>
                            <img src="img/webdevelopment.jpg" alt="Web Development" class="img-fluid">
                            <p class="pt-3">We are a professional Web Development and Designing company with highly skilled professionals with a good knowledge on;</p>

                            <ul class="list-items">
                                <li>HTML</li>
                                <li>Word Press</li>
                                <li>Magento</li>
                                <li>Open Cart</li>
                                <li>Joomla</li>                                
                            </ul>

                            <p>Our Professionals understand your requirement. We provide Website designing solutions to </p>

                            <ul class="list-items">
                                <li>Small Business Houses</li>
                                <li>E-Commerce Website Designing</li>
                                <li>Corporate Web Designing</li>
                                <li>Web Application Development</li>                                                        
                            </ul>
                            <p>Our Development Services are;</p>                           

                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-3">
                                    <img src="img/graphics/ux-design.svg" alt="" class="img-fluid w-100">
                                    <h5 class="h6 fbold text-center py-2">UX Design</h5>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-3">
                                    <img src="img/graphics/responsive-design.svg" alt="" class="img-fluid w-100">
                                    <h5 class="h6 fbold text-center py-2">Responsive</h5>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-3">
                                    <img src="img/graphics/illustration.svg" alt="" class="img-fluid w-100">
                                    <h5 class="h6 fbold text-center py-2">Illustration</h5>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-3">
                                    <img src="img/graphics/wireframing-design.svg" alt="" class="img-fluid w-100">
                                    <h5 class="h6 fbold text-center py-2">Wireframing</h5>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->



                           
                        </div>
                        <!-- /Web development -->
                        
                        <!-- MI ML AR-->
                        <div class="tab_content">
                            <h3 class="h4 fbold tabtitle">Artificial Intelligence, Machine Learning, Natural Language Processing, Augmented and Virtual Reality</h3>
                            <img src="img/machine-learning.jpg" alt="" class="img-fluid">                          

                             <!-- new content -->
                            <p>Artificial Intelligence- Artificial intelligence (AI) is the simulation of human intelligence processes by machines, especially computer systems. Specific applications of AI include expert systems, natural language processing (NLP), speech recognition and machine vision.</p>

                            <p>AI focuses on three cognitive skills;</p>

                            <ul class="list-items">
                                <li><span class="fbold">Learning processes </span> This aspect of AI programming focuses on acquiring data and creating rules for how to turn the data into actionable information. The rules, which are called algorithms, provide computing devices with step-by-step instructions for how to complete a specific task.</li>
                                <li><span class="fbold">Reasoning processes </span> This aspect of AI programming focuses on choosing the right algorithm to reach a desired outcome.</li>
                                <li><span class="fbold">Self-correction processes </span> This aspect of AI programming is designed to continually fine-tune algorithms and ensure they provide the most accurate results possible.</li>
                            </ul>

                            <div class="text-center">
                                <img src="img/graphics/iot.svg" alt="" class="img-fluid w-100">
                            </div>

                            <p>We offer AI in below service areas</p>

                            <ul class="list-items">
                                <li>AI in Health Care</li>
                                <li>AI in Govt. Sectors (Forest and Agriculture)</li>
                                <li>AI in manufacturing</li>
                                <li>AI in Banking and Financial Institutions</li>
                                <li>AI in securities</li>
                                <li>AI hospitality industries</li>
                            </ul>                          
                                <!-- col -->
                                <div class="row">                                   
                                    <!-- col -->
                                    <div class="col-lg-8">
                                    <h3 class="h5 py-2 fbold">Machine Learning</h3>
                                    <p>Machine Learning is the field of study that gives computers the capability to learn without being explicitly programmed. ML is one of the most exciting technologies that one would have ever come across. As it is evident from the name, it gives the computer that makes it more similar to humans: <i>The ability to learn.</i> Machine learning is actively being used today, perhaps in many more places than one would expect.</p>
                                    <p>Machine learning is a tool for turning information into knowledge. In the past 50 years, there has been an explosion of data. This mass of data is useless unless we analyse it and find the patterns hidden within. Machine learning techniques are used to automatically find the valuable underlying patterns within complex data that we would otherwise struggle to discover. The hidden patterns and knowledge about a problem can be used to predict future events and perform all kinds of complex decision making.</p>
                                    </div>
                                <!--/ col -->
                                    <!-- col -->
                                    <div class="col-lg-4 align-self-center">
                                    <img src="img/machine-learing2.jpg" alt="" class="img-fluid">
                                </div>
                                <!--/ col -->                                
                            </div>
                            <!--/ row -->

                            <p>There are multiple forms of Machine Learning; supervised, unsupervised , semi-supervised and reinforcement learning. Each form of Machine Learning has differing approaches, but they all follow the same underlying process and theory. This explanation covers the general Machine Leaning concept and then focusses in on each approach.</p>

                            <h3 class="h5 py-2 fbold">Terminology</h3>

                            <ul class="list-items">
                                <li><span class="fbold">Dataset:</span> A set of data examples, that contain features important to solving the problem.</li>
                                <li><span class="fbold">Features: </span> Important pieces of data that help us understand a problem. These are fed in to a Machine Learning algorithm to help it learn.</li>
                                <li><span class="fbold">Model: </span> The representation (internal model) of a phenomenon that a Machine Learning algorithm has learnt. It learns this from the data it is shown during training. The model is the output you get after training an algorithm. For example, a decision tree algorithm would be trained and produce a decision tree model.</li>                               
                            </ul>     

                            <h3 class="h5 py-2 fbold">Process</h3>

                            <ul class="list-items">
                                <li><span class="fbold">Data Collection:</span> Collect the data that the algorithm will learn from.</li>
                                <li><span class="fbold">Data Preparation: </span> Format and engineer the data into the optimal format, extracting important features and performing dimensionality reduction.</li>
                                <li><span class="fbold">Training: </span> Also known as the fitting stage, this is where the Machine Learning algorithm actually learns by showing it the data that has been collected and prepared.</li>       
                                <li><span class="fbold">Evaluation: </span> Test the model to see how well it performs.</li>        
                                <li><span class="fbold">Tuning: </span> Fine tune the model to maximise it’s performance.</li>                      
                            </ul>     

                            <h3 class="h5 py-2 fbold">Augmented Reality (AR) and Virtual Reality (VR)</h3>

                           <p>Augmented reality (AR) is a technology that layers computer-generated enhancements atop an existing reality in order to make it more meaningful through the ability to interact with it. AR is developed into apps and used on mobile devices to blend digital components into the real world in such a way that they enhance one another, but can also be told apart easily.</p>
                           <p>AR technology is quickly coming into the mainstream. It is used to display score overlays on telecasted sports games and pop out 3D emails, photos or text messages on mobile devices. Leaders of the tech industry are also using AR to do amazing and revolutionary things with holograms and motion activated commands.</p>
                           <img src="img/vr.jpg" alt="" class="img-fluid pb-2">
                          
                           <p>Virtual reality (VR) is an artificial, computer-generated simulation or recreation of a real life environment or situation. It immerses the user by making them feel like they are experiencing the simulated reality firsthand, primarily by stimulating their vision and hearing.</p>
                           <p>VR is typically achieved by wearing a headset equipped with the technology, and is used prominently in two different ways:</p>

                           <ul class="list-items">
                                <li>To create and enhance an imaginary reality for gaming, entertainment, and play (Such as video and computer games, or 3D movies, head mounted display).</li>
                                <li>To enhance training for real life environments by creating a simulation of reality where people can practice beforehand (Such as flight simulators for pilots).</li>                               
                            </ul>  
                            <p>Virtual reality is possible through a coding language known as VRML (Virtual Reality Modeling Language) which can be used to create a series of images, and specify what types of interactions are possible for them.</p> 
                            <!--/ new content -->

                        </div>
                        <!-- /MI ML AR-->

                        <!-- BI Solutions -->
                        <div class="tab_content">
                            <h3 class="h4 fbold tabtitle">BI Solutions</h3>
                            <img src="img/biimage.jpg" alt="" class="img-fluid">
                            <h3 class="h5 py-2 fbold">Turn Complex Data into Powerful Business Intelligence</h3>
                            <p class="fbold">Believe and discover the power of Data- We Transform your Data into Business Insights for Easy Decision Making and Predictive Analysis.</p>

                            <p>We, focuse on providing consulting services to clients in the areas of Data Management, Data Integration, Data Migration & Business Intelligence. We like to do business as “partners in success” and strongly believe that our success is a function of the success of our clients. We aspire to be the consulting firm of choice for cost and quality conscious clients and become an “on demand” consulting firm that can provide optimal solutions to our clients’ complex challenges.</p>

                            <div class="row">
                                <div class="col-lg-6">
                                    <img src="img/graphics/bi-solutions.svg" alt="" class="img-fluid w-100">
                                </div>
                                <div class="col-lg-6">
                                    <p class="fbold">Business intelligence software and systems</p>
                                    <p>A variety of different types of tools we use for providing best of the solutions to our clients with following features:</p>
                                    <ul class="list-items">
                                        <li>Dashboards</li>
                                        <li>Visualizations</li>
                                        <li>Reporting</li>
                                        <li>Data mining</li>
                                        <li>ETL (extract-transfer-load —tools that import data from one data store into another)</li>
                                        <li>OLAP (online analytical processing)</li>
                                    </ul>                                    
                                </div>
                            </div>
                            <p>OUR Services are</p>
                            <ul class="list-items">
                                <li>Data Management</li>
                                <li>Data Warehousing</li>
                                <li>Data Integration</li>
                                <li>Business Intelligence</li>                                
                            </ul>
                        </div>
                        <!--/ BI solutions -->
                      				
                    </div>
		        </div>
                <!--/ tab -->
            </div>
            <!--/container -->
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->
    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>