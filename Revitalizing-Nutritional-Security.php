<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iDream Tech</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>Blog</h1>
                        <p> News &amp; Updates </p>
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="blog.php">Blog</a></li>
                            <li class="active"><a>Revitalizing Nutritional Security</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">

        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-9">
                    <img src="img/blogs/blog01.jpg" alt="" class="img-fluid w-100 mb-2">
                    <article class="py-3">
                        <h4 class="h4 fbold">                           
                            Revitalizing Nutritional Security                                                     
                        </h4>
                        <p class="small"><i class="fblue">Posted on 03-04-2020 by Admin</i></p>
                        <h5 class="h5 fgray">We develop facilities to nurture the INSECTs, the Superfood for Sustainable Aquaculture & Poultry industry</h5>
                        
                        <p class="fblue">(Biological waste to protein pack animal feed for better environment, nutritional security, local employment)</p>
                        <p>As urbanization continues unabated due to migration for better employment and living; by 2050 three billion more people are expected to be living in cities. While greater affordability and availability will positively improve the dietary habits, the natural outcome is that there will be an increased demand for high calorie and protein diet.</p>
                        <p>India is one of the largest and fastest growing compound feed markets in the world. Indian feed industry is presently growing at a CAGR of 8 percent. Poultry, aqua and dairy industry occupy the major share in overall feed demand.</p>
                        <p>We plan and shape industrial mode to protein pack INSECT rearing from Biological waste. These insects are not much to look at, but they are making major waves in the aquaculture & poultry industry. These superstar larvae are disrupting the fish feed status quo by providing a protein-packed, inexpensive and high-quality alternative to the otherwise unsustainable feeds on the market.</p>
                        <p>These unassuming little powerhouses have an extremely high protein content, fast growth rate, and will feast on just about anything, including human waste and cadavers.</p>
                        <p>By replacing fish feed and fish oil with INSECTs, farmers will be changing the process by which they use wild fish to feed farmed fish, thus eliminating reliance on wild stocks. In effect, they are revolutionizing the aquaculture industry with greater employment opportunities for the local youths and ecological balance.</p>
                        <p>Israel being the leading agri-tech country in the world and having expertise has left foot prints for multi million dollar “Waste to Wealth”products. And we are lucky to have tied up with few experts in this area for providing services as best global practices. Highly dense cities and states in India are really need to adopt such technologies for long run sustainability.</p>
                        <p>Few More to go in this series… </p>
                    </article>
                    
                </div>
                <!--/ col -->

                <!-- right col -->
                <?php include 'moreblogs.php'?>
                <!--/ right col -->

            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
          

            

           

           
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->       

    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>