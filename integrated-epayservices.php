<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Integrated Epay services</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>Integrated Epay System</h1>                        
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="products-all.php">Products</a></li>
                            <li class="active"><a href="javascript:void(0)">Integrated Epay System</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 aos-item" data-aos="fade-up">                       
                        <img src="img/graphics/e-pay-solutions-functionalities.svg" alt="" class="img-fluid w-100">

                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-down">                
                        <h3 class="h4 py-2 fbold">Our Integrated E-Pay web service</h3>
                        <!-- <p>Using our web service, you can manage all your payments outside of ePay's system. This means you can manage payments from your online shop without logging in to ePay's administration.</p>
                        <p>Our API is a practical tool for processing your payments. For instance, you can manage your orders/payments from one place or set up automatic withdrawals when you ship the order.</p> -->

                        <p>In Digital world a growing need is for a seamless and secure electronic payment to optimize the customer experience and increase conversion rate. The processing of such payments need to take place online, real-time, 24/7 all year around, in a safe and secure way.</p>

                        <p>Our e-Payments solution provides services for the main online payment schemes at door-steps in India and enables easy integration with corporate customers or public organizations. We have an integrated solutions to cater it for Smart Cities.</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container --> 
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->
    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>