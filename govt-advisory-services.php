<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Govt. Advisory Services</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>Govt. Advisory Services</h1>                        
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="products-all.php">Services</a></li>
                            <li class="active"><a href="javascript:void(0)">Govt. Advisory Services</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">

                <!-- product row -->
                <div class="row product-list-item py-sm-2">
                    <!-- col -->
                    <div class="col-lg-12 aos-item" data-aos="fade-up" >
                        <p>We at iDream provide Advisory services with the goal to support undertakings and overcome weakness in specific areas like  Govt. Polices and Consulting Services, finance, business, legal and Operational. We have a range of business advisors bestowing best-in-class services to help organizations perform up to the ballpark and become a sovereign. Our wide range of experience and aggregated services cater to different ranges of clients starting from Start-ups to Matured Business Houses to Govt. Services.</p>
                    </div>
                    <!--/ col -->                   
                </div>
                <!-- product row -->



                <!-- tab -->
               <div class="parentHorizontalTab">
                    <ul class="resp-tabs-list hor_1">
                        <li>e-Governance Project </li>
                        <li>Smart City Solutions </li>
                        <li>Office Automation</li>
                        <li>Other Consulting Services</li>
                    </ul>
                    <div class="resp-tabs-container hor_1">

                        <!-- e-Governance Project-->
                        <div>                      
                            <!-- child vertical tab -->
                            <div class="ChildVerticalTab_1">
                                <ul class="resp-tabs-list ver_1">
                                    <li>Co-Operative Society Solutions </li>
                                    <li>Banking Solutions</li>
                                    <li>Urban Development Solutions</li>
                                    <li>e-SHG</li>
                                    <li>Digital India </li>
                                </ul>
                                <div class="resp-tabs-container ver_1">
                                    <!-- Co-Operative Society Solutions-->
                                    <div>
                                        <h3 class="h4 fbold tabtitle">Co-Operative Society Solutions</h3>
                                        <img src="img/co-operative-society-solutions.jpg" alt="" class="img-fluid">
                                        <p class="pt-2">A Co-Operative society is a voluntary organization or association started with an aim of providing services to its members. These are generally formed by the under privileged people or weaker section people in the society. cooperative societies providing their banking services with the focus on rural and suburban part of India. Shreshta a conglomerate of iDream Tech has a decade old experience in providing multiple technical supports to Co-Operative Societies and Banks across India.</p>

                                        <p>Shreshta provides an enterprise application build on a common architecture using common data , logic and a set of business process. We offer below services;</p>

                                        <ul class="list-items">
                                            <li>Accounts (Ledger Book,Recon Book,Vendor Management,GL)</li>
                                            <li>E-POS (Andriod and other utility services with few citizen centric APIs integrated into it.)</li>
                                            <li>Inventory Management System, Warehouse Management System, Etc.</li>
                                            <li>HRMS and office Automation</li>
                                            <li>Customer Relationship Management (CRM)</li>
                                            <li>Third Party Module</li>
                                            <li>Catalog and Product Management</li>
                                        </ul>
                                    </div>
                                    <!--/ Co-Operative Society Solutions-->

                                    <!-- banking soclutions -->
                                    <div>
                                         <h3 class="h4 fbold tabtitle">Structure of Cooperative Banking</h3>
                                         <img src="img/banking-solutions.jpg" alt="" class="img-fluid">
                                         <p class="pt-2">There are different types of cooperative credit institutions working in India. These institutions can be classified into two broad categories- agricultural and non-agricultural. Agricultural credit institutions dominate the entire cooperative credit structure.</p>
                                         <p>Agricultural credit institutions are further divided into short-term agricultural credit institutions and long-term agricultural credit institutions.</p>
                                         <p>The short-term agricultural credit institutions which cater to the short-term financial needs of agriculturists have three-tier federal structure-</p>

                                         <ul class="list-items">
                                            <li>At the apex, there is the state cooperative bank in each state;</li>
                                            <li>At the district level, there are central cooperative banks;</li>
                                            <li>At the village level, there are primary agricultural credit societies.</li>
                                         </ul>

                                         <p>Long-term agricultural credit is provided by the land development banks. The whole structure of cooperative credit institutions is shown in the chart given.</p>
                                    </div>
                                    <!--/ banking solutions -->

                                    <!-- urban development solutions -->
                                    <div>
                                        <h3 class="h4 fbold tabtitle">Urban Development Solutions</h3>
                                        <p>iDream Tech provides urban and regional strategic planning and consulting services. Our sustainability and urban renewal team of experts includes experienced urban planners, environmental consultants and economists, who believe that education, community, economics and the environment are bonded together.</p>
                                        <p>Lead by team of urban planning experts from Israel and India, we provide national and global solutions to social, economic and technological projects, enabling sustainable transitions.</p>
                                       
                                        <h3 class="h5 fbold graytxt">1. SSOP - Solid Sludge to Oil Processing - Creating an Economy out of Waste</h3>
                                        
                                        <p>We in Idream Tech tied up with few global experts in providing Rural and Urban Development Solutions towards a sustainable economy. Our SSOP technology supports any kind of Sludges - Industrial, Sweage, Solid, etc.</p>

                                        <p>Our SSOP solution is cheaper than traditional disposal methods by at least one fifth up to a maximum of one tenth, which includes installation and operation costs, while transportation cost are none existent for SSOP.</p>
                                       
                                        <img src="img/graphics/operational-framework.svg" alt="" class="img-fluid w-100">
                                       
                                        <ul class="list-items">
                                            <li>Municipalities</li>
                                            <li>Wastewater treatment facilities</li>
                                            <li>Industrial plants</li>
                                            <li>Biopharmaceutical / Biotechnology companies</li>
                                            <li>Global corporations</li>
                                        </ul>

                                        <h3 class="h5 fbold ">How Does It Work</h3>
                                        <img src="img/ssop-howitworks.jpg" alt="" class="img-fluid">


                                        <h3 class="h5 fbold graytxt">2. Solid Waste Management </h3>
                                        <img src="img/solid-waste-management.jpg" alt="" class="img-fluid">
                                     

                                        <h4 class="h4 fbold">Solid Waste Management life cycle</h4>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <img src="img/solid-waste-management2.jpg" class="img-fluid" alt="">
                                            </div>
                                            <div class="col-lg-6">
                                                <p>iDream Tech provides urban and regional strategic planning and consulting services. Our sustainability and urban renewal team of experts includes experienced urban planners, environmental consultants and economists, who believe that education, community, economics and the environment are bonded together.</p>

                                                <p>Lead by team of urban planning experts from Israel and India, we provide national and global solutions to social, economic and technological projects, enabling sustainable transitions. </p>
                                            </div>
                                        </div>

                                        <p>Our Services </p>
                                        <ul class="list-items">
                                            <li>Urban and regional socio-economic planning</li>
                                            <li>Circular economy like waste management towards wealth creation </li>
                                            <li>Environment &amp; renewable energy consulting</li>
                                            <li>Research and development</li>
                                            <li>Feasibility studies of business and social projects </li>
                                            <li>Strategic planning &amp; public policy consulting</li>
                                            <li>Industrial and urban renewal projects  </li>
                                            <li>IOT based data monitoring and Dashboard </li>
                                        </ul>

                                        <h3 class="h5 fbold graytxt">3. Green House Solutions</h3>
                                        <img src="img/greenhousing.jpg" alt="" class="img-fluid w-100">
                                        <p class="fbold">Green Housing- A Sustainable Solution</p>
                                        <p>We at iDream provide you an Innovative Housing Solution. Innovative houses are featured with Passive air conditioning, rain water harvesting and resistant to earthquake.</p>
                                        <p>We also visualise to integrate adequate infrastructure like playground, sewerage, drainage, education, health, community space for business and social gathering with provision of additional funds towards sustainable community development.</p>
                                        <h3 class="h5 py-2">Unique features of our greenhouse model </h3>
                                        <ul class="list-items">
                                            <li>Energy efficient </li>
                                            <li>Renewable Heat & cool </li>
                                            <li>Solar Energy </li>
                                            <li>Smart Electronics</li>
                                            <li>Water conservation </li>
                                            <li>Green Roofs / Walls</li>
                                            <li>Integrated waste management  </li>
                                            <li>Food (Backyard / front yard kitchen garden) towards nutrition security </li>
                                            <li>Green Streets and Neighbourhoods</li>
                                            <li>Affordable Housing</li>
                                        </ul>
                                    </div>
                                    <!-- urbhan development solutions-->

                                    <!-- e-shg -->
                                    <div>
                                        <h3 class="h4 fbold tabtitle">e-SHG</h3>
                                        <img src="img/eshg.jpg" alt="" class="img-fluid mb-2">
                                        <p>Self Help Groups are the intermediary committees in rural India bridging the gap between Govt. Machineries and citizen. These groups work as intermediaries in livelihood programs to financial aid programs of govt. or other agencies. We try to digitize these SHG groups providing POS machines, White Level ATMs, Computers, Creating a digital network between SHGs,etc. and make it a self-sustainable model.</p>
                                    </div>
                                    <!--/ e-shg-->

                                    <!-- digital india -->
                                    <div>
                                        <h3 class="h4 fbold tabtitle">Digital India</h3>
                                        <img src="img/application-development.jpg" alt="" class="img-fluid">
                                        <p class="pt-2">iDream Tech is part of the digital India mission providing a smart India solution through knowledge economy. With Emerging technologies we are leaping towards a new tech-era in digitizing, automating and creating smart environments. Idream Tech has below capabilities to run with Smart timing and knowledge economy;</p>
                                        <ul class="list-items">
                                            <li>Office Automation (e-Office)</li>
                                            <li>Conference  and call recordings as notes (Phone, skype or any other medium of communication can be translated into a note through AI for further reference)</li>
                                            <li>Digital Media Management and Social Media Analystice for message transformation</li>
                                            <li>Skill Development- Emerging Technologies, Digital Media Management</li>
                                        </ul>
                                    </div>
                                    <!--/ digital india -->
                                </div>
                            </div>
                            <!-- child vertical tab --> 
                        </div>
                        <!--/ e-Governance Project-->
                       
                       <!-- Smart City Solutions-->
                        <div>
                            <!-- child vertical tab -->
                            <div class="ChildVerticalTab_1">
                                <ul class="resp-tabs-list ver_1">
                                    <li>Surveillance </li>
                                    <li>Digital Money</li>
                                    <li>Property Tax Collection</li>
                                    <li>IoT Solutions</li>
                                   
                                </ul>
                                <div class="resp-tabs-container ver_1">
                                    <!--Surveillance-->
                                    <div>
                                        <h3 class="h4 fbold tabtitle">Surveillance</h3>    
                                        <img src="img/survilance.jpg" alt="" class="img-fluid mb-2">
                                        <p>As per the Information and communication Technologies (ICT) policy of Govt. of India, it’s mandatory for all the cities and urban bodies to improve the efficiency and sustainability of urban spaces while reducing costs and resource consumption. As part of the smart city concept City Surveillance is important to monitor citizens through strategically placed sensors around the urban landscape, which collect data regarding many different factors of urban living. From these sensors, data is transmitted, aggregated, and analysed by governments and other local authorities in order to extrapolate information about the challenges the city faces in sectors such as</p>
                                        <ul class="list-items">
                                            <li>Crime prevention,</li>
                                            <li>Traffic management,</li>
                                            <li>Energy use and waste reduction. </li>
                                        </ul>
                                        <p>This serves to facilitate better urban planning and allows governments to customise their services to the local population. </p>
                                        <p>We have an advantage being an multiple OEM partners to provide best of the solutions as per the client’s requirement .We provide below services;</p>
                                        <ul class="list-items">
                                            <li>CCTVs (options of renewable energy sources)</li>
                                            <li>Data Center and Command Center Set up </li>
                                            <li>Data Archiving and Monitoring Services</li>
                                        </ul>
                                    </div>
                                    <!--/Surveillance-->

                                    <!-- digital money -->
                                    <div>

                                        <h3 class="h4 fbold tabtitle">Digital Money</h3>    
                                        <img src="img/digital-payments.jpg" alt="" class="img-fluid mb-2">
                                        <p><span class="fbold">e-Pos- </span>Smart/android POS and Kiosks embedded with multiple utility services API for providing a door step solutions for citizens as part of the digitization. This would not only save time also, create multiple skilled opportunities for citizens as a new venture. We offer below services for easing human lives;</p>

                                        <ul class="list-items">
                                            <li>Utility Payments- Electricity Bills, Water Bills,Etc.</li>
                                            <li>Booking Train, Bus, Flight Tickets </li>
                                            <li>Money Transfer Facilities</li>
                                            <li>Money Withdrawal facilities</li>
                                            <li>Loan Payment and loan Processing </li>
                                        </ul> 

                                    </div>                                   
                                    <!--/ digital money -->

                                    <!-- Property Tax Collection-->
                                    <div>
                                        <h3 class="h4 fbold tabtitle">Property Tax Collection</h3>    
                                        <p><span class="fbold">Property Tax Collection </span>Property Tax is the principal source of revenue for Urban Local Bodies in virtually every part of the  world. In most cities in India the property tax base has been considerably eroded by administrative  and procedural inadequacies.</p>                                      

                                        <!-- ROW -->
                                        <div class="row">
                                            <!-- col -->
                                            <div class="col-lg-12">
                                            <p>We provide an end-to-end smart property tax collection solutions for all Urban bodies (Municipalities and corporations).We propose a structured approach to the development & implementation, using standard  implementation Methodology. The implementation cycle comprises five phases:</p>
                                             <!-- col -->
                                             <div class="col-lg-12">
                                                <img src="img/graphics/property-tax-collection.svg" class="img-fluid w-100" alt="">
                                            </div>
                                            <!--/ colk -->
                                            <p>We will develop a Web based Online System for the following.</p>
                                            <ul class="list-items">
                                                <li>Property/House Tax Collection Module</li>
                                                <li>Water Charges Collection Module </li>
                                                <li>Trade License Fee Collection Module</li>                                       
                                            </ul>
                                            </div>
                                            <!--/ col -->
                                           
                                        </div>
                                        <!-- .ROW -->

                                        <!-- <ul class="list-items">
                                            <li>Phase 1 – Project Preparation</li>
                                            <li>Phase 2 – Business Blueprint </li>
                                            <li>Phase 3 – Realization</li>
                                            <li>Phase 4 – Final Preparation</li>
                                            <li>Phase 5 – Go-Live and Support </li>
                                        </ul> -->
                                        <!-- <img src="img/propertytax-collection.jpg" alt="" class="img-fluid"> -->

                                        
                                    </div>
                                    <!-- /Property Tax Collection-->

                                    <!-- iot solutions -->
                                    <div>
                                        <h3 class="h4 fbold tabtitle">IoT solutions</h3>    
                                        <p><span class="fbold">IOT Solutions</span>The "Internet of things" (IoT) is becoming an increasingly growing topic of conversation both in the workplace and  outside of it. It's a concept that not only has the potential to impact how we live but also how we work.</p>
                                        <p>The IoT is impacting the value chain of every business today. It is changing the way products &  services are conceptualized, created, processed and delivered..</p>
                                        <p>Connecting virtually all things sensors, gateways, machines, devices, animals, people—IoT  Technology makes it possible to connect to the Cloud, enabling sound decisions and making  people’s lives better.</p>

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <img src="img/graphics/iot02.svg" alt="" class="img-fluid">
                                        </div>
                                        <div class="col-lg-6 align-self-center">
                                            <ul class="list-items">
                                                <li>Smart Metering</li>
                                                <li>Environmental Safety </li>
                                                <li>Personal Safety</li>                                       
                                                <li>Employee Health & Safety</li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-6 order-lg-last">
                                            <img src="img/graphics/iot03.svg" alt="" class="img-fluid">
                                        </div>
                                        <div class="col-lg-6 align-self-center">
                                            <ul class="list-items">
                                                <li>Smart Lighting</li>
                                                <li>Assest Management  </li>
                                                <li>Smart Feedback</li>
                                            </ul>
                                        </div>
                                    </div>

                                    </div>
                                    <!-- iot solutions -->
                                </div>

                            </div>
                            <!-- child vertical tab --> 
                        </div>
                        <!--/ Smart City Solutions-->
                        
                        <!-- Office Automation-->
                        <div>
                           <!-- child vertical tab -->
                           <div class="ChildVerticalTab_1">
                                <ul class="resp-tabs-list ver_1">
                                    <li>e-Office </li>
                                    <li>HRMS</li>                                   
                                </ul>
                                <div class="resp-tabs-container ver_1">

                                    <!-- e-Office-->
                                    <div>
                                        <h3 class="h4 fbold tabtitle">e-Office</h3>    
                                        <img src="img/e-office.jpg" alt="" class="img-fluid">
                                        <p>eOffice is a Mission Mode Project (MMP) under the National e-Governance Programme of the Government. aiming to usher more efficient, effective and transparent inter-government and intra-government transactions and processes.</p>
                                        <p>Benefits of eOffice</p>
                                        <ul class="list-items">
                                            <li>Enhance transparency </li>
                                            <li>Increase accountability   </li>
                                            <li>Assure data security and data integrity </li>
                                            <li>Promote innovation by releasing staff energy and time from unproductive procedures </li>
                                            <li>Transform the government work culture and ethics</li>
                                        </ul>
                                        <p>We Offer below modules;</p>
                                        <ul class="list-items">
                                            <li>FMS- File Management System/File Tracking System </li>
                                            <li>Knowledge Sharing and Tracking System   </li>
                                            <li>Messaging Services System </li>
                                            <li>File Archiving System</li>                                           
                                        </ul>
                                    </div>
                                    <!--/ e-Office-->
                                    <!-- hrms-->
                                    <div>
                                        <h3 class="h4 fbold tabtitle">HRMS</h3>    
                                        <img src="img/humanresrouce-management.jpg" alt="" class="img-fluid">
                                        <p>Human Resources Management System is an interface between processes that connect Human Resource and information Technology through HR Software. This automates the entire work place and helps a soothing work place solution. This reduces repetitive calculations or overlapping processes in turn saves lots of time.</p>
                                        <p>Our HRMS modules are one of the best proven models and here are the modules we offer;</p>

                                        <ul class="list-items">
                                            <li>Pay Roll Management System </li>
                                            <li>Leave Management System   </li>
                                            <li>Recruitment and On-Boarding System </li>
                                            <li>Learning and Development Calendar </li>   
                                            <li>Benefits Administration</li>                                        
                                            <li>Employee Scheduling</li>
                                            <li>Employee Appraisal System</li>
                                            <li>Feedback and Grievance Handling System</li>
                                        </ul>

                                    </div>      
                                    <!--/ hrms-->                             
                                </div>
                            </div>
                            <!-- child vertical tab --> 
                        </div>
                        <!--/ Office Automation-->

                        <!-- Other Consulting Services-->
                        <div>
                            <!-- child vertical tab -->
                            <div class="ChildVerticalTab_1">
                                <ul class="resp-tabs-list ver_1">
                                    <li>Man Power Solutions  </li>
                                    <li>Network Solutions</li>
                                    <li>Cloud Computing Services </li>                                                           
                                </ul>
                                <div class="resp-tabs-container ver_1">
                                    <!-- man power solutions -->
                                    <div>
                                        <h3 class="h4 fbold tabtitle">Man Power Solutions </h3>    
                                        <img src="img/manpower-solutions.jpg" alt="" class="img-fluid">
                                        <p>We in iDream Tech have more than 100 years of cumulative experience in Manpower Solutions and we have a strong out sourcing capability. Our Service offerings are as follows;</p>
                                        <ul class="list-items">
                                            <li>Manpower Recruitment- Technical and Non-Technical, All levels (0-50 yrs. of experience). We have a strong pool of talent database. </li>
                                            <li>Staffing- Contract, Permanent, Project and Pool Based.  </li>
                                            <li>Third party Pay roll management and Compensation Benefit Planning.</li>
                                        </ul>

                                    </div>
                                    <!--/ man power solutions -->

                                    <!-- network solutions -->
                                    <div>
                                        <h3 class="h4 fbold tabtitle">Network & Infrastructure Service </h3>   
                                        <p>Growth in the Solution Integration (SI) services market is fuelled by the need for seamless business processes across  an organization’s complete value chain of customers, partners, suppliers, and employees. iDream's services enable  clients to identify, develop, and implement the best-fit solutions which are equipped to meet their changing business  requirements.</p>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <img src="img/network-solutions.jpg" alt="" class="img-fluid">
                                            </div>
                                            <div class="col-lg-6">
                                                <p>iDream Tech's Solution Integration services offer to:</p>
                                                <ul class="list-items">
                                                    <li>Leverage IT investments </li>
                                                    <li>Minimize risks </li>
                                                    <li>Maximize compatibility</li>
                                                    <li>Maximize interoperability</li>
                                                </ul>
                                                <p>iDream Tech provides total project management, right from architectural design, integration, system and interface  development to migration backed by world-class methodologies, well-defined solution frameworks and extensive  integration experience with tier-1 service providers.</p>
                                            </div>
                                            
                                        </div>
                                    </div> 
                                    <!--/ network solutions -->

                                    <!-- Cloud Computing -->
                                    <div>
                                        <h3 class="h4 fbold tabtitle">Cloud Computing Services </h3> 
                                        <!-- <img src="img/cloud-computing.jpg" alt="" class="img-fluid w-100 mb-2"> -->
                                        <!-- table -->
                                        <div class="table-responsive custom-table">
                                            <table class="table">
                                                <thead class="thead-dark">
                                                    <tr class="iconrow">
                                                        <th scope="col">Packaged Software Own Datacenter</th>
                                                        <th scope="col">IaaS  <br> (Infrastructure as a Service) </th>
                                                        <th scope="col">PaaS  <br> (Platform as a Service) </th>
                                                        <th scope="col">SaaS  <br> (Service as a Service) </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr class="iconrow">
                                                        <td><span class="icon-arrow-down icomoon"></span></td>
                                                        <td><span class="icon-arrow-down icomoon"></span></td>
                                                        <td><span class="icon-arrow-down icomoon"></span></td>
                                                        <td><span class="icon-arrow-down icomoon"></span></td>
                                                    </tr>
                                                    <tr>
                                                        <td>Applications</td>
                                                        <td>Applications</td>
                                                        <td>Applications</td>
                                                        <td class="tdblue">Applications</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Data &amp; Security </td>
                                                        <td>Data &amp; Security </td>
                                                        <td>Data &amp; Security </td>
                                                        <td class="tdblue">Data</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Runtime</td>
                                                        <td>Runtime </td>
                                                        <td class="tdgreen">Runtime </td>
                                                        <td class="tdblue">Runtime</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Middleware</td>
                                                        <td>Middleware </td>
                                                        <td class="tdgreen">Middleware </td>
                                                        <td class="tdblue">Middleware</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Operating System</td>
                                                        <td>Operating System</td>
                                                        <td class="tdgreen">Operating System</td>
                                                        <td class="tdblue">Operating System</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Virtualization</td>
                                                        <td class="tdaquablue">Virtualization</td>
                                                        <td class="tdgreen">Virtualization</td>
                                                        <td class="tdblue">Virtualization</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Server (Hardware)</td>
                                                        <td class="tdaquablue">Server (Hardware)</td>
                                                        <td class="tdgreen">Server (Hardware)</td>
                                                        <td class="tdblue">Server (Hardware)</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Storage</td>
                                                        <td class="tdaquablue">Storage</td>
                                                        <td class="tdgreen">Storage</td>
                                                        <td class="tdblue">Storage</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Networking &amp; Security </td>
                                                        <td class="tdaquablue">Networking &amp; Security </td>
                                                        <td class="tdgreen">Networking &amp; Security </td>
                                                        <td class="tdblue">Networking &amp; Security </td>
                                                    </tr>
                                                    <tr class="iconrow">
                                                        <td><span class="icon-arrow-down icomoon"></span></td>
                                                        <td><span class="icon-arrow-down icomoon"></span></td>
                                                        <td><span class="icon-arrow-down icomoon"></span></td>
                                                        <td><span class="icon-arrow-down icomoon"></span></td>
                                                    </tr>
                                                </tbody>
                                                <tfoot>
                                                    <tr class="iconrow">
                                                        <td>Managed by Owner</td>
                                                        <td>Managed by Cloud Vendor</td>
                                                        <td>Managed by Cloud Vendor</td>
                                                        <td>Managed by Cloud Vendor</td>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                        <!--/ table -->
                                        <p>Cloud Computing refers to accessing and sharing of information in the internet web space, rather than using our personal computers, hard drives, and local servers. The term ‘Cloud’ refers to an Internet web space. </p>
                                        <p>Cloud computing provides everything as services over the internet. It will allow us to access and manage every service over the internet from anywhere. Cloud Technology is nothing but the combination of Datacenter or multiple Data centers connected by the network with each other to share resources dynamically that comprises computation, Storage, and software. All these resources  are virtualized into one big platform  intelligently and automatically configured to make easy use of everything as a service. </p>                                       
                                       
                                    </div>
                                    <!--/ Cloud Computing -->
                                </div>                                
                            </div>
                            <!-- child vertical tab --> 
                        </div>
                        <!--/ Other Consulting Services-->
                    </div>
                </div>
                <!--/ tab -->
            </div>
            <!--/container -->
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->
    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>