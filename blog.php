<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iDream Tech</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>Blog</h1>
                        <p> News &amp; Updates </p>
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li class="active"><a>Blog</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">

        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-4 col-sm-6">
                    <div class="blog-col">
                        <a href="Revitalizing-Nutritional-Security.php">
                            <img src="img/blogs/blog01.jpg" alt="" class="img-fluid">
                        </a>
                        <article>
                            <h6 class="h5 fbold">
                                <a href="Revitalizing-Nutritional-Security.php">
                                    Revitalizing Nutritional Security
                                </a>                                
                            </h6>
                            <p>We develop facilities to nurture the INSECTs, the Superfood for Sustainable Aquaculture & Poultry industry</p>
                        </article>
                    </div>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->
          

            

           

           
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->       

    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>