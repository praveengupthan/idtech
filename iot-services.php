<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>IoT Services</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>IoT Services</h1>                        
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="#">Services</a></li>
                            <li class="active"><a href="javascript:void(0)">IoT Services</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-down">
                        <img src="img/IoTimg01.jpg" class="img-fluid" alt="">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-up">     
                        <p>The Internet of Things is an emerging area of technical, social, and economic significance. Consumer products, durable goods, cars and trucks, industrial and utility components, sensors, and other everyday objects are being combined with Internet connectivity and powerful data analytic capabilities that promise to transform the way we work, live, and play. Projections for the impact of IoT on the Internet and economy are impressive, with some anticipating as many as 100 billion connected IoT devices and a global economic impact of more than $11 trillion by 2025. </p>  
                        <p>Our end-to-end IoT solutions empower smart industries, smart life Style and smart business houses. Also it enriches experiences by connecting assets, operations/logistics, and services.</p>  
                       
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                  <!-- row -->
                  <div class="row py-sm-3">
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item order-lg-last" data-aos="fade-down">
                        <img src="img/graphics/iot01.svg" class="img-fluid w-100" alt="">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-up">     
                       
                        <p> While we have expertise across all stages of IoT adoption to provide you with a seamless single vendor experience to maximize efficiencies, we focus predominantly on the segments mentioned below: </p>                       
                        <ul class="list-items">
                            <li> <span class="fbold">Smart Industries – </span> Manufacturing, Energy, Utilities. Re-imagine processes to unlock the true potential of your industry while facilitating sustainable development. </li>
                            <li> <span class="fbold">Smart Life Style – </span> Wearables, Healthcare, Security. Enhance the quality of life by embracing emerging technologies designed to foster healthier, happier and safe environment.</li>
                            <li> <span class="fbold"> Smart Business Houses – </span> Smart homes/ Buildings/ Offices, Retail. Connect people, machines and information using Big Data to enhance business efficiency in a secured ecosystem.</li>
                            
                        </ul>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row justify-content-center py-md-3">
                   <!-- col -->
                   <div class="col-lg-12 text-center">
                        <img src="img/graphics/IoT-lifecycle.svg" class="img-fluid w-100 aos-item" alt="" data-aos="fade-up">
                   </div>
                   <!--/ col -->
                </div>
                <!--/ row -->

                  <!-- row -->
                  <div class="row py-md-3">
                    <!-- col -->
                    <div class="col-lg-12 text-center">
                        <h4 class="h4 fbold">IoT Solution Approach</h4>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-4">
                        <h3 class="h4 ">Process</h3>  
                        <ul class="list-items">
                            <li>Requirement analysis and business alignment.</li>
                            <li>Competitive analysis.</li>
                            <li>Anticipating new use cases.</li>
                            <li>Tools and framework selection.</li>
                        </ul>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-4">
                        <h3 class="h4 ">Implementation</h3>  
                        <ul class="list-items">
                            <li>Platform design.</li>
                            <li>Design blueprinting.</li>
                            <li>Solution prototyping.</li>
                            <li>Agile and continuous delivery.</li>
                            <li>Developer enablement services.</li>
                        </ul>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-4">
                        <h3 class="h4 ">Quality</h3>  
                        <ul class="list-items">
                            <li>Data testing using firmware.</li>
                            <li>Simulation-based testing.</li>
                            <li>Data comprehensiveness and accuracy testing.</li>
                            <li>Interruption testing.</li>
                            <li>Component certification.</li>
                        </ul>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row py-md-3">
                    <!-- col -->
                    <div class="col-lg-12 text-center">
                        <h4 class="h4 fbold">Our Solutions</h4>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                
                 <!-- row -->
                 <div class="row py-md-3">
                    <!-- col -->
                    <div class="col-lg-3 text-center aos-item" data-aos="fade-up">
                        <img src="img/iotsol01.jpg" alt="" class="img-fluid">
                        <h5 class="h6 fbold py-md-3">Digital Twin</h5>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3 text-center aos-item" data-aos="fade-up">
                        <img src="img/iotsol02.jpg" alt="" class="img-fluid">
                        <h5 class="h6 fbold py-md-3">Prevision Maintenance</h5>
                    </div>
                    <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-3 text-center aos-item" data-aos="fade-up">
                        <img src="img/iotsol03.jpg" alt="" class="img-fluid">
                        <h5 class="h6 fbold py-md-3">Smart Agriculture</h5>
                    </div>
                    <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-3 text-center aos-item" data-aos="fade-up">
                        <img src="img/iotsol04.jpg" alt="" class="img-fluid">
                        <h5 class="h6 fbold py-md-3">Smart City</h5>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3 text-center aos-item" data-aos="fade-up">
                        <img src="img/iotsol05.jpg" alt="" class="img-fluid">
                        <h5 class="h6 fbold py-md-3">Smart Waste Management</h5>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3 text-center aos-item" data-aos="fade-up">
                        <img src="img/iotsol06.jpg" alt="" class="img-fluid">
                        <h5 class="h6 fbold py-md-3">Enterprise IoT</h5>
                    </div>
                    <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-3 text-center aos-item" data-aos="fade-up">
                        <img src="img/iotsol07.jpg" alt="" class="img-fluid">
                        <h5 class="h6 fbold py-md-3">Truck Fleet Management</h5>
                    </div>
                    <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-3 text-center aos-item" data-aos="fade-up">
                        <img src="img/iotsol08.jpg" alt="" class="img-fluid">
                        <h5 class="h6 fbold py-md-3">RFID - Asset Tracking</h5>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                
                  <!-- row -->
                  <div class="row py-md-3">
                    <!-- col -->
                    <div class="col-lg-12 text-center">
                        <h4 class="h4 fbold">Key Factors of our IoT Solutions</h4>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-4">
                        <h3 class="h4 ">Iot End Points</h3>  
                        <p>Covers the physical world and operational technology required to connect things and communicate.</p>
                        <ul class="list-items">
                            <li>Things(firmware and non-IP connectivity).</li>
                            <li>Sensors</li>
                            <li>Actuators</li>
                            <li>Agents</li>
                            <li>Edge computing device.</li>
                            <li>Communication protocols.</li>
                        </ul>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-4">
                        <h3 class="h4 ">IoT Software</h3>  
                        <p>Our  IoT software services together to manage the IoT endpoints securely, represent the digital twin of connected things, process and analyze data, and provide APIs to consume and expose services.</p>
                        <ul class="list-items">
                            <li>Device management.</li>
                            <li>Digital twin management.</li>
                            <li>Event and data processing.</li>
                            <li>Analytics or machine learning.</li>
                            <li>API management.</li>
                        </ul>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-4">
                        <h3 class="h4 ">IoT Apps</h3>  
                        <p>The Applications layer is where IoT solutions are brought to life, turning data into actionable insight, putting it in the hands of our users, customers and partners.</p>
                        <ul class="list-items">
                            <li>Integrated development environment (IDE).</li>
                            <li>Multichannel applications.</li>
                            <li>Integration</li>
                            <li>Testing.</li>
                            <li>Deployment.</li>
                        </ul>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                  <!-- row -->
                  <div class="row py-md-3">
                    <!-- col -->
                    <div class="col-lg-12 text-center">
                        <h4 class="h4 fbold">Our Products </h4>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                
                 <!-- row -->
                 <div class="row py-md-3">
                    <!-- col -->
                    <div class="col-lg-3 text-center">
                        <img src="img/iotroduct01.jpg" alt="" class="img-fluid">
                        <h5 class="h6 fbold py-md-3">IP Cores</h5>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3 text-center">
                        <img src="img/iotroduct02.jpg" alt="" class="img-fluid">
                        <h5 class="h6 fbold py-md-3">Smart City</h5>
                    </div>
                    <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-3 text-center">
                        <img src="img/iotproduct04.jpeg" alt="" class="img-fluid">
                        <h5 class="h6 fbold py-md-3">IoT Product Kit</h5>
                        
                    </div>
                    <!--/ col -->

                      <!-- col -->
                      <div class="col-lg-3 text-center">
                        <img src="img/iotroduct03.jpg" alt="" class="img-fluid">
                        
                        <h5 class="h6 fbold py-md-3">CleverBell</h5>
                    </div>
                    <!--/ col -->

                   
                </div>
                <!--/ row -->


            </div>
            <!--/ container --> 
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->
    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>