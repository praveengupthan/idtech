   <!-- header -->
   <header>
        <!-- container -->
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand p-0" href="index.php"><img src="img/logo.png" alt="iDream Tech"></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span class="icon-menu icomoon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'select-nav';}else {echo'nav-link';} ?>" href="index.php">Home </a>
                        </li>
                        <li class="nav-item">
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='about.php'){echo'select-nav';}else {echo'nav-link';} ?>" href="about.php">About us</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle <?php if(            
                                basename($_SERVER['SCRIPT_NAME']) == 'cooperative-society-management.php' ||      
                                basename($_SERVER['SCRIPT_NAME']) == 'integrated-epayservices.php' || 
                                basename($_SERVER['SCRIPT_NAME']) == 'smart-virtual-classes.php' ||                  
                                basename($_SERVER['SCRIPT_NAME']) == 'hrms-solutions.php' || 
                                basename($_SERVER['SCRIPT_NAME']) == 'school-management.php'||
                                basename($_SERVER['SCRIPT_NAME']) == 'healthcare-automation.php'||
                                basename($_SERVER['SCRIPT_NAME']) == 'products-all.php')
                                {echo 'select-nav'; } else { echo 'nav-link'; } ?>" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Products
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'cooperative-society-management.php'){echo 'dropdown-active'; }else { echo 'dropdown-item'; } ?>" href="cooperative-society-management.php">Co-Operative Society Management</a>
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'integrated-epayservices.php'){echo 'dropdown-active'; }else { echo 'dropdown-item'; } ?>" href="integrated-epayservices.php">Integratd E-Pay Services</a>
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'smart-virtual-classes.php'){echo 'dropdown-active'; }else { echo 'dropdown-item'; } ?>" href="smart-virtual-classes.php">Smart and Virtual Classes</a>                               
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'hrms-solutions.php'){echo 'dropdown-active'; }else { echo 'dropdown-item'; } ?>" href="hrms-solutions.php">HRMS Solutions</a>
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'school-management.php'){echo 'dropdown-active'; }else { echo 'dropdown-item'; } ?>" href="school-management.php">School Management</a>
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'healthcare-automation.php'){echo 'dropdown-active'; }else { echo 'dropdown-item'; } ?>" href="healthcare-automation.php">Health Care Automation</a>                               
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'products-all.php'){echo 'dropdown-active'; }else { echo 'dropdown-item'; } ?>" href="products-all.php">View All Products</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle <?php if(            
                                basename($_SERVER['SCRIPT_NAME']) == 'it-solutions.php' ||      
                                basename($_SERVER['SCRIPT_NAME']) == 'ites.php' || 
                                basename($_SERVER['SCRIPT_NAME']) == 'management-consulting.php' ||                  
                                basename($_SERVER['SCRIPT_NAME']) == 'govt-advisory-services.php' || 
                                basename($_SERVER['SCRIPT_NAME']) == 'iot-services.php')
                                {echo 'select-nav'; } else { echo 'nav-link'; } ?>" href="#" id="navbarDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Services
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'it-solutions.php'){echo 'dropdown-active'; }else { echo 'dropdown-item'; } ?>" href="it-solutions.php">IT Solutions</a>
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'ites.php'){echo 'dropdown-active'; }else { echo 'dropdown-item'; } ?>" href="ites.php">ITES</a>
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'management-consulting.php'){echo 'dropdown-active'; }else { echo 'dropdown-item'; } ?>" href="management-consulting.php">Management Consulting</a>
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'govt-advisory-services.php'){echo 'dropdown-active'; }else { echo 'dropdown-item'; } ?>" href="govt-advisory-services.php">Govt Advisory Services</a>  
                                <a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'iot-services.php'){echo 'dropdown-active'; }else { echo 'dropdown-item'; } ?>" href="iot-services.php">IoT Services</a>                             
                            </div>
                        </li>
                        
                        <li class="nav-item">
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='blog.php'){echo'select-nav';}else {echo'nav-link';} ?>" href="blog.php">Blog</a>
                        </li>
                        <li class="nav-item">
                            <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='contact.php'){echo'select-nav';}else {echo'nav-link';} ?>" href="contact.php">Contact</a>
                        </li>
                    </ul>
                    <a class="icon-nav"><img src="img/msme-logo.jpg"></a>

                </div>
            </nav>
        </div>
        <!--/ container -->
    </header>
    <!--/ header -->