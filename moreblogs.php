<div class="col-lg-3 related-blogs">
    <h4 class="h5 fbold border-bottom mb-3">More Blogs</h4>
    <div class="related-in">
        <div class="blog-col">
            <a href="Revitalizing-Nutritional-Security.php">
                <img src="img/blogs/blog01.jpg" alt="" class="img-fluid">
            </a>
            <article>
                <h6 class="h6 fbold ">
                    <a href="Revitalizing-Nutritional-Security.php">
                        Revitalizing Nutritional Security
                    </a>                                
                </h6>                            
            </article>
        </div>
    </div>
</div>