<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iDream Tech</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>Co Operative Society Management</h1>                        
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="products-all.php">Products</a></li>
                            <li class="active"><a href="javascript:void(0)">Co Operative Society Management</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
        <!-- container -->
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6 aos-item" data-aos="fade-up">
                    <img src="img/graphics/cooperative-society-management-system.svg" class="img-fluid w-100" alt="">
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 align-self-center aos-item" data-aos="fade-down">
                <p>iDream Tech offers you hassle-free platform to transform your society, making it highly functional and easily manageable. Developed by an expert team with over 25years of experience handling society matters, this software enables even a newcomer to participate in the management process</p>
                <h3 class="h4 py-2 fbold">Manage Accounts</h3>
                <p>Modular Accounting System enables any individual, familiar or unfamiliar with accounting to easily manage multiple activities simultaneously while maintaining society accounts</p>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ container -->  

        <!-- categories -->
        <div class="whitebox py20">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-8 aos-item" data-aos="fade-up">                        
                         <img src="img/graphics/co-operative-what-we-offcer.svg" alt="" class="img-fluid w-100">
                    </div>
                    <!-- /col -->

                     <!-- col -->
                     <div class="col-lg-8 aos-item" data-aos="fade-down">                        
                         <img src="img/graphics/application-categories.svg" alt="" class="img-fluid w-100">
                    </div>
                    <!-- /col -->                    
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!-- /categories -->
        
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->
    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>