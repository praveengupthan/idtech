<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Smart and Virtual Classes</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>Smart and Virtual Classes</h1>                        
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="products-all.php">Products</a></li>
                            <li class="active"><a href="javascript:void(0)">Smart and Virtual Classes</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-up">
                        <img src="img/graphics/smartclasses.svg" class="img-fluid" alt="">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-down">     
                        <p>Smart Classrooms are Classrooms (version 2.0) where teaching and learning effectiveness is enhanced by integrating learning technology, such as computers, specialized software, audience response technology, assistive listening devices, networking, and audio/visual capabilities.</p>           
                        <h3 class="h4 py-2 fbold">Advantages</h3>
                        <ul class="list-items">
                            <li>Better Student- Teacher interaction. </li>
                            <li>Introduction of technology for students &amp; teachers.</li>
                            <li>Perfect mix of learning &amp; teaching.</li>
                            <li>Better clarity for  students in understanding concepts.</li>
                            <li>Faster &amp; improved learning abilities through creativity.</li>
                            <li>Improved Academic performance of students</li>
                            <li>Provides students a better in-classroom experience.</li>
                            <li>Easy to use and share reading content &amp; materials.</li>
                            <li>Perfect for students with Low IQ</li>
                            <li>Addition of the fun element in learning</li>
                        </ul>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                  <!-- row -->
                  <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 align-self-center order-lg-last aos-item" data-aos="fade-down">
                        <img src="img/graphics/smartclasses2.svg" class="img-fluid" alt="">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-up">     
                        <p>Virtual Classroom is an e-learning concept which helps the teachers and the students to impart and receive education in an online mode, over the Internet.</p>  

                        <p>VCs allow both parties to communicate and interact together from any location, without actually being physically present : face-to-face,.</p>   
                        <p>Virtual Classes can be conducted through:</p>       
                        <ul class="list-items">
                            <li>Webinars </li>
                            <li>Audio and video conferences</li>
                            <li>Web presentations</li>
                            <li>Live streaming</li>
                            <li>Text chats </li>
                            <li>Learning Management System (LMS) </li>
                            <li>Online Training courses. </li>
                        </ul>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->


                  <!-- row -->
                  <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-up">
                        <img src="img/graphics/smartclasses3.svg" class="img-fluid" alt="">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-down">     
                        <h3 class="h4 py-2 fbold">Grow With us!</h3>
                        <ul class="list-items">
                            <li>Core Team into the IT Busines for last 20 years Have ground understanding of requirement and concern of the End user </li>
                            <li>Our Smart class Product / Solutions are easy to use and maintain. easily adoptable to even novice user</li>
                            <li>We have Sophisticated solutions which can integrate with existing hardware. We ensure optimal utilization of resources.</li>
                            <li>Customize solutions to address customer requirement. Solutions are flexwible to user prerequisite.</li>
                            <li>Being brand owner we have complete Eco-system to support and server for long term. </li>
                        </ul>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

            </div>
            <!--/ container --> 
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->
    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>