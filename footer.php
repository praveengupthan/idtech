
    <!-- footer -->
    <footer>

        <!-- top footer -->
        <div class="top-footer">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-8 text-center">                      

                        <ul class="nav justify-content-center pt-2">
                            <li class="nav-item">
                                <a href="index.php" class="nav-link">Home</a>
                            </li>
                            <li class="nav-item">
                                <a href="about.php" class="nav-link">About us</a>
                            </li>
                            <li class="nav-item">
                                <a href="products-all.php" class="nav-link">Products</a>
                            </li>
                            <li class="nav-item">
                                <a href="it-solutions.php" class="nav-link">Services</a>
                            </li>                           
                            <li class="nav-item">
                                <a href="contact.php" class="nav-link">Contact</a>
                            </li>
                        </ul>

                        <!-- social links-->
                        <div class="social-links d-flex justify-content-center text-center">
                            <a href="https://www.facebook.com/iDreamTechOffl" target="_blank">
                                <span class="icon-facebook icomoon"></span>
                            </a>
                            <a href="https://www.linkedin.com/company/idreamtechoffl" target="_blank">
                                <span class="icon-linkedin icomoon"></span>
                            </a>
                            <a href="https://twitter.com/iDreamTechOffl" target="_blank">
                                <span class="icon-twitter icomoon"></span>
                            </a>
                            <a href="https://www.instagram.com/idreamtechoffl" target="_blank">
                                <span class="icon-instagram icomoon"></span>
                            </a>
                        </div>
                        <!--/ social links-->
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ top footer -->

        <!-- bottom footer -->
        <div class="bottom-footer text-center py-3">
            <p>copy rights reserved 2020@idreamtech.net</p>
        </div>
        <!--/ bottom footer -->
    </footer>
    <!--/ footer -->