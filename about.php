<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iDream Tech</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>About us</h1>
                        <p>“iDream believes in creating Innovative, Pragmatic and Commitment based dreams” </p>
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li class="active"><a>About us</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
            <img src="img/about01.jpg" alt="" class="img-fluid">

            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row shadow-box justify-content-center minus-col mb-md-5 mx-0">
                        <!-- col -->
                        <div class="col-lg-10 py-md-3">
                              <h2 class="section-title aos-item" data-aos="fade-up">Who we are</h2>
                              <p class="text-md-justify aos-item" data-aos="fade-up">iDream Tech is a subsidiary IT Solutioning company of iDream Media Inc. based out of New Jersey, USA. We are an End –to-End solution provider with more than 150 years of collective experience in IT and Management Consulting, IT Services (Hardware and Software), IT Enabled Services(ITeS) and Government Advisory Services. We have expertise across all the phases of SDLC, Management Consulting, BPR and can deliver high quality products/services with optimum utilization of project parameters.</p>
                              <p class="text-md-justify aos-item" data-aos="fade-up">We are an MSME and startup Registered IT solutioning Company</p>
                        </div>
                        <!--/ col -->
                </div>
                <!--/row -->

                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-4 aos-item whatvdo " data-aos="fade-down">
                        <div class="shadow-box p-md-5 minus-right">
                            <h3 class="section-title">What we do</h2>
                            <p>We are committed to provide world class services and solutions to our customers. We develop smart applications and consulting services in line with customer requirements and to reap business values for them. iDream is one of the fastest growing media company across globe and we are following the foot step in our Tech division too. iDream Tech in India works as an independent entity that provides Comprehensive IT Solutions, ITeS, Management Consulting and Advisory Services to meet business needs at both domestic and overseas client’s.</p>
                            <p>We are an IT Product aggregation having multiple service Providers and Solutioning Company for a digital revolution and Single Point of delivery.</p>
                        </div>
                    </div>
                    <!--/col -->
                    <!-- col -->
                    <div class="col-lg-8 aos-item align-self-center" data-aos="fade-up">
                        <img src="img/board-meeting.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/col -->
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row pt-lg-4">
                    <!-- col -->
                    <div class="col-lg-6 aos-item whatvdo order-lg-last align-self-center" data-aos="fade-down">
                        <div class="shadow-box p-md-5">
                            <h3 class="section-title">Strength</h2>
                            <ul class="list-items">
                                <li>Experience (150+ years collective)</li>
                                <li>Network (Globally across Sectors)</li>
                                <li>Professional Approach</li>
                                <li>Desire for Adopting and Adapting New Age Technologies</li>                               
                            </ul>
                        </div>
                    </div>
                    <!--/col -->
                    <!-- col -->
                    <div class="col-lg-6 aos-item align-self-center" data-aos="fade-up">
                        <img src="img/strength.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/col -->
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 aos-item whatvdo align-self-center" data-aos="fade-down">
                        <div class="shadow-box p-md-5">                           

                            <h3 class="section-title">Our Opportunities</h2>
                            <ul class="list-items">
                                <li>New Age Technologies</li>
                                <li>Digital India Movement</li>
                                <li>Creating a Second line Advisory and Management Consulting Services</li>
                                <li>Transparency towards customers</li>                               
                            </ul>
                        </div>
                    </div>
                    <!--/col -->
                    <!-- col -->
                    <div class="col-lg-6 aos-item align-self-center" data-aos="fade-up">
                        <img src="img/opportunities.jpg" alt="" class="img-fluid">
                    </div>
                    <!--/col -->
                </div>
                <!--/ row -->

                <!-- vision and mission new row -->
                <div class="row py-4">
                    <!-- col -->
                    <div class="col-lg-4 text-center vcol">
                        <div class="icon">
                            <span class="icon-goal icomoon"></span>
                        </div>
                        <h2 class="h4 fbold py-md-2">Vision</h2>
                        <p class="text-center">To the competent and complete value added service Provider to our client’s.</p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-4 text-center vcol">
                        <div class="icon">
                            <span class="icon-development icomoon"></span>
                        </div>
                        <h2 class="h4 fbold py-md-2">Mission</h2>
                        <p class="text-center">To deliver the right business solutions that integrate People, Process and Emerging Technology to help our client's to achieve the business benefits of a collaborative enterprise..</p>
                    </div>
                    <!--/ col -->
                     <!-- col -->
                     <div class="col-lg-4 text-center vcol">
                        <div class="icon">
                            <span class="icon-crm icomoon"></span>
                        </div>
                        <h2 class="h4 fbold py-md-2">Values</h2>
                        <p class="text-center">To be transparent, committed and adaptable to our client’s.</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ vision and missin new row -->

               
               
               
            </div>
            <!--/ container -->

            <!-- container team -->
            <div class="container">
                <!-- row -->
                <div class="row justify-content-center py20">
                    <!-- col title -->
                    <div class="col-lg-6 text-center">
                        <h2 class="section-title">Our Management Team</h2>
                    </div>
                    <!--/ col title -->
                </div>
                <!--/ row -->

                <!-- team row -->
                <div class="team-row mb-3">
                    <!-- row -->
                    <div class="row">
                        <!-- cool -->
                        <div class="col-lg-3 col-md-4 align-self-sm-center aos-item text-center text-sm-left" data-aos="fade-up">
                            <img src="img/vasu-founder.jpg" alt="" class="img-fluid teampic ml-md-3">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-9 col-md-8 align-self-center aos-item text-sm-left" data-aos="fade-down">
                            <article class="p-sm-4">
                                <h6 class="h6 pb-0">Chinna Vasudeva Reddy</h6>                               
                                    <small class="fblue fbold">
                                       Chairman & Managing director
                                    </small> 
                                <p class="py-md-2">Commonly known as CVR is the Founder and CEO at iDream Media Inc. and founded companies like CVR Idream Tech and iDream Infra in India. An MS in Computer Sciences having an incredible contribution in the field of IT and Social Media. But his strong connection to the roots and culture, encouraged him to become a techpreneur. His desire to contribute to the improvement of the lives of the downtrodden people of India have resulted in a brief stint in start-up promotion, prevention of non-communicable diseases, waste to wealth, innovative greenhouse & promote circular economy to reduce the impact of climate change. The convergence of Technology, Entertainment, effort towards reduction of climate change impact put him in a unique position, to pursue value creation as well as innovation. His effort to promote & retain Indian culture in different countries across the globe, make him unique.  </p>

                                <ul class="nav">
                                    <li class="nav-item">
                                        <a href="https://www.facebook.com/ChinnaVasudevaReddy/" class="nav-link" target="_blank">
                                            <img src="img/fb-icon.svg" alt="">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="jhttps://twitter.com/chinnavasudeva?lang=en" class="nav-link" target="_blank">
                                            <img src="img/twitter.svg" alt="">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="linkedin.com/in/vasudeva-reddy-chinna-7481331a" class="nav-link" target="_blank">
                                            <img src="img/linkedin.svg" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </article>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/row -->
                </div>
                <!--/ team row -->

                 <!-- team row -->
                 <div class="team-row mb-3">
                    <!-- row -->
                    <div class="row">
                        <!-- cool -->
                        <div class="col-lg-3 col-md-4 align-self-center order-lg-last aos-item text-sm-center pr-4" data-aos="fade-up">
                            <img src="img/ajay-pic.jpg" alt="" class="img-fluid teampic position-relative">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-9 col-md-8 align-self-sm-center aos-item" data-aos="fade-down">
                            <article class="p-sm-4">
                                <h6 class="h6 pb-0">Ajay Kumar Mohanty</h6>                               
                                <small class="fblue fbold">
                                    Chief Executive Officer (CEO)
                                </small> 

                                <p class="py-md-2">A Finance and PPP professional with over 19 years of experience into Financial planning and analysis, Secretarial Practice across industries such as Govt. and Social Sector, Metals & Mineral, Retail, Manufacturing, IT& ITES, Services industry and Transportation industry. Over the year has developed business acumen to analyse and report multi country operations. Played instrumental role in developing turnaround, and long range strategy for companies. Adept with necessary tools and methodologies for Business Intelligence, Market Intelligence, strategy formulation, and business planning for diversified business organizations. Expertise in undertaking and supporting exercises pertaining to New business ideation, business expansion, new product development, value addition, and market development.</p>

                                <ul class="nav">
                                    <li class="nav-item">
                                        <a href="https://www.facebook.com/profile.php?id=100048422683877 " class="nav-link" target="_blank">
                                            <img src="img/fb-icon.svg" alt="">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="https://twitter.com/ajumohanty" class="nav-link" target="_blank">
                                            <img src="img/twitter.svg" alt="">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="https://www.linkedin.com/in/ajayfpa/" class="nav-link" target="_blank">
                                            <img src="img/linkedin.svg" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </article>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/row -->
                </div>
                <!--/ team row -->
                
                <!-- team row -->
                <div class="team-row mb-3">
                    <!-- row -->
                    <div class="row">
                        <!-- cool -->
                        <div class="col-lg-3 col-md-4 align-self-sm-center aos-item text-sm-center" data-aos="fade-up">
                            <img src="img/prasanna-img.jpg" alt="" class="img-fluid teampic ml-md-3">
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-9 col-md-8 align-self-center aos-item" data-aos="fade-down">
                            <article class="p-sm-4">
                                <h6 class="h6 pb-0">Barada Prasanna Mohanty</h6>                               
                                    <small class="fblue fbold">
                                        Director
                                    </small>                                

                                <p class="py-md-2">Barada an MBA Techpreneur from Odisha, eastern part of India having a baggage with more than 20 years of experience into IT Consulting and IT Solutions Provider across India. Has a proven track record in project management and delivery, Government Consulting and Advisory Services, New Product Development and implementation, Business Development and expansion in his career. Leading Sreshta Consulting Services Pvt. Ltd,(SCPL) as a domain expert in Co-Operative Society Automation, HRMS and other e-Gov. projects across India.</p>

                                <ul class="nav">
                                    <li class="nav-item">
                                        <a href="javascript:void(0)" class="nav-link">
                                            <img src="img/fb-icon.svg" alt="">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:void(0)" class="nav-link">
                                            <img src="img/twitter.svg" alt="">
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:void(0)" class="nav-link">
                                            <img src="img/linkedin.svg" alt="">
                                        </a>
                                    </li>
                                </ul>
                            </article>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/row -->
                </div>
                <!--/ team row -->
            </div>
            <!--/ container team -->

            <!-- container -->
            <!-- <div class="container">
                <img src="img/Start-Up-Certificate.png" alt="" class="img-fluid aos-item" data-aos="fade-down">
            </div> -->
            <!--/ container -->
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->       

    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>