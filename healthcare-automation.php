<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Health Care Automation</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>Health Care Automation</h1>                        
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="products-all.php">Products</a></li>
                            <li class="active"><a href="javascript:void(0)">Health Care Automation</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-2">
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-up">
                        <img src="img/azeero-img.jpg" class="img-fluid" alt="">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-down">     
                        <h3 class="h4 py-2 fbold">What is aazeero P2P Automation</h3>
                        <p>aazeero P2P automation is the patented and the only automated AI, IoT, and Cloud-based technology for Procurement to Pricing Automation in healthcare on the planet. It takes care of everything all that matters to the healthcare organizations between Procurement to Pricing automation. It’s a B2B, B2B2C and BYOC (Buy Your Own Cloud) technology for Healthcare Organizations and Patients to drastically minimizes the spend and saving more lives</p>     
                       
                        <h3 class="h4 py-2 fbold">ERP Services</h3>
                        <p>Access and manage ocean of information in just 5 widgets – Any layman can use the system, No need to buy a huge ERP like oracle Financials or SAP or other high priced and heavy weight technologies- Go light and save lots of money and lives </p>

                        <p>No need to buy any additional infrastructure and make huge investments to use our technology- all in cloud, safe, secure and accessible all the time, No need to spend lots of healthcare productive hours for training as it’s designed to at most user experience with the latest technologies </p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row py-2">                   
                    <!-- col -->
                    <div class="col-lg-12 align-self-center aos-item" data-aos="fade-up">     
                        <h3 class="h4 py-2 fbold">Modules</h3>
                        <ul class="list-items">
                            <li><span class="fbold">Procurement - </span> Invented and fully automated procurement technology for all the goods and services through auctioning and bidding using AI technologies to avoid corruption, save lots money, save lots of time and faster replacements without human interventions</li>
                            <li><span class="fbold">Scientific Procedure Costing – </span> AI and Scientific costing for all kinds of Procedures, Lab Tests and many more. </li>
                            <li><span class="fbold"> Pricing Automation – </span> AI and Scientific pricing for for all kinds of healthcare services in an organization. E.g. Patient Rooms, Consultation Rooms, Operation Theatres, ICU’s, many more
                            </li>
                            <li><span class="fbold"> Resource Management/Booking- </span> E booking of  Patient Rooms, Consultation Rooms, Operation Theatres, ICU’s, many more </li>
                            <li><span class="fbold"> Appointment Booking - </span> E booking of doctors appointments, consultation appointments, etc. Healthcare services booking and patient registration through Aadhaar or any permissible Govt ID cards or through biometric data to save time, get rid of errors and save more lives
                            </li>
                            <li><span class="fbold"> Procedure Lifecycle Management – </span> End to end management of Procedures for patients.   </li>
                            <li><span class="fbold"> Treatment Tracking and Follow-ups - </span> seamlessly connects with patients and doctors Planning follow-ups for every procedure digitally.  </li>
                            <li><span class="fbold"> Prescription and Patient Data Management – </span> Digitization of patient data, prescriptions, etc. on the cloud to be accessible on demand with the underlying security and safety protocols
                            </li>
                            <li><span class="fbold"> Smart Inventory  - </span> Inventory management through Smart Load cells using IOT technology will require no humans – secured and Error Free Inventory  management      (Also available without IOT )
                            </li>
                            <li><span class="fbold"> Vendor Management – </span> Vendor management pertaining to all the department of the healthcare organization   </li>
                            <li><span class="fbold"> Customer Management – </span> B2B Feature manage their customers/Hospitals/Healthcare Organizations/etc  </li>
                            <li><span class="fbold"> Invoices – </span> Automated Invoices </li>
                            <li><span class="fbold"> Billing – </span> Adapters to push data to existing systems or new to billing systems and basic billing of the current system  </li>
                            <li><span class="fbold"> Review  and Rating – </span> Review and rating of every service that an healthcare organization provides ( starting from the human resources ( Vendors, Doctors, Healthcare Staff, etc.), Machines, OT’s, services, etc.
                            </li>
                        </ul>            
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container --> 
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->
    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>