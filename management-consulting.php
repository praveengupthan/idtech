<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Management Consulting</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>Management Consulting</h1>                        
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="products-all.php">Services</a></li>
                            <li class="active"><a href="javascript:void(0)">Management Consulting</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- product row -->
                <div class="row product-list-item py-sm-2">
                    <!-- col -->
                    <div class="col-lg-12 align-self-center text-center">
                        <h2 class="h2 text-center">We believe in Adopting “Best Practices” and guide you for the same.</h2>
                        <p class="text-center">We Dream to provide our client best of the Management Solutions in increasing their Performance, Delivery Management Services, Financial Solutions, Networking Support Globally. Also we have an expertise in creating strategic tie-ups with International Marketing Agencies to help you grow and deliver, Finding a Funding Partner based on your business and long-term goals to expand.</p>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-12 aos-item" data-aos="fade-down">
                       <img src="img/graphics/management-consulting.svg" alt="" class="img-fluid aos-item" data-aos="fade-up">
                    </div>
                    <!--/ col -->                    
                </div>
                <!-- product row -->              
            </div>
            <!--/container -->
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->
    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>