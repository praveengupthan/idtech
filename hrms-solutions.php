<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>HRMS Solutions</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>HRMS Solutions</h1>                        
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="products-all.php">Products</a></li>
                            <li class="active"><a href="javascript:void(0)">HRMS Solutions</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row py-2">
                    <!-- col -->
                    <div class="col-lg-6 align-self-center" data-aos="fade-up">                       
                        <h3 class="h4 py-2 fbold">Specialized HR software</h3>
                        <p>Specialized HR software focuses on one module or HR function, such as recruiting or payroll. It typically has advanced functionality and many features. It's generally offered by smaller vendors that can focus on incorporating emerging technology into their platforms. Best-of-breed solutions in the HR market are usually specialized software.</p>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-down">     
                        <h3 class="h4 py-2 fbold">WHAT IS HRMS?</h3>
                        <p>A human resource management system (HRMS) is comprehensive software that integrates core and strategic HR functions into one solution. It typically features an employee self-service portal and a centralized database. It also automates administrative processes, streamlines recruiting and reduces turnover.</p> 
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-11 mx-auto">
                        <img src="img/graphics/hrms-servicebook-updating.svg" class="img-fluid w-100" alt="">
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                 <!-- row -->
                 <div class="row py-2">
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-up">
                        <img src="img/graphics/hrsoftware.svg" class="img-fluid" alt="">
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-down">     
                        <h3 class="h4 py-2 fbold">Comprehensive HR software</h3>
                        <p>Comprehensive HR software (often referred to as HRMS, HRIS or HCM software) covers many modules and has broad functionality. It's generally provided by large software vendors that have stayed competitive over the years by acquiring and integrating smaller vendors and specialized software. It benefits companies that want a single, integrated solution to cover their HR needs.</p>    
                       
                      
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->                
            </div>
            <!--/ container --> 
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->
    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>