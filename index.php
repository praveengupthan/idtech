<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iDream Tech</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>

    <?php include 'header.php' ?>
    <!-- main -->
    <main>
        <!-- home slider -->
        <div class="home-slider">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <!-- container -->
                        <div class="container">
                            <!-- content -->
                            <div class="row contentrow justify-content-center">
                                <!-- col -->
                                <div class="col-lg-6 align-self-center col-sm-8">
                                    <article>
                                        <h2 class="aos-item" data-aos="fade-down">IT <span>Solutions </span></h2>
                                        <p class="aos-item" data-aos="fade-up">Our customized enterprise solutions can assist you to reduce operational
                                            costs,
                                            extend capabilities, and sustain the profitability through integrating
                                            automation in your business processes.</p>
                                        <p>
                                            <a href="it-solutions.php" class="idlink aos-item" data-aos="fade-up">
                                                Read More <span class="icon-arrow-right icomoon"></span>
                                            </a>
                                        </p>
                                    </article>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-lg-6 text-center col-sm-4">
                                    <img src="img/slider01.png" class="slider-img img-fluid">
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ content -->
                        </div>
                        <!--/ container -->
                    </div>
                    <div class="carousel-item">
                        <!-- container -->
                        <div class="container">
                            <!-- content -->
                            <div class="row contentrow justify-content-center">
                                <!-- col -->
                                <div class="col-lg-6 col-sm-8 align-self-center">
                                    <article>
                                        <h2 class="aos-item" data-aos="fade-up"><span>ITES</span></h2>
                                        <p class="aos-item" data-aos="fade-up">We understand that being in the ITES industry, your company can encounter a
                                            wide
                                            variety of risks and challenges in their endeavor to create and maintain a
                                            seamless, successful, sustainable and scalable business.</p>
                                        <p class="aos-item" data-aos="fade-up">
                                            <a href="ites.php" class="idlink">
                                                Read More <span class="icon-arrow-right icomoon"></span>
                                            </a>
                                        </p>
                                    </article>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-lg-6 col-sm-4 text-center">
                                    <img src="img/slider02.png" class="slider-img img-fluid">
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ content -->
                        </div>
                        <!--/ container -->
                    </div>

                    <div class="carousel-item">
                        <!-- container -->
                        <div class="container">
                            <!-- content -->
                            <div class="row contentrow justify-content-center">
                                <!-- col -->
                                <div class="col-lg-6 col-sm-8 align-self-center">
                                    <article>
                                        <h2>Management <span>Consulting </span></h2>
                                        <p>We are committed to delivering value to our clients and ensuring long-term
                                            success. Reach out to us to improve your business performance, innovate and
                                            grow, reduce costs, leverage talent and transform your organisation.</p>
                                        <p>
                                            <a href="management-consulting.php" class="idlink">
                                                Read More <span class="icon-arrow-right icomoon"></span>
                                            </a>
                                        </p>
                                    </article>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-lg-6 col-sm-4 text-center">
                                    <img src="img/slider03.png" class="slider-img img-fluid">
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ content -->
                        </div>
                        <!--/ container -->
                    </div>

                    <div class="carousel-item">
                        <!-- container -->
                        <div class="container">
                            <!-- content -->
                            <div class="row contentrow justify-content-center">
                                <!-- col -->
                                <div class="col-lg-6 col-sm-8 align-self-center">
                                    <article>
                                        <h2>Govt. <span>Advisory Services </span></h2>
                                        <p>Our Advisory teams use local and global knowledge to help you challenge
                                            conventions and introduce and deliver strategies that work specifically for
                                            you.
                                        </p>
                                        <p>
                                            <a href="govt-advisory-services.php" class="idlink">
                                                Read More <span class="icon-arrow-right icomoon"></span>
                                            </a>
                                        </p>
                                    </article>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-lg-6 col-sm-4 text-center">
                                    <img src="img/slider04.png" class="slider-img img-fluid">
                                </div>
                                <!--/ col -->

                            </div>
                            <!--/ content -->
                        </div>
                        <!--/ container-->
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="icon-chevron-left icomoon"></span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="icon-chevron-right icomoon"></span>
                </a>
            </div>
        </div>
        <!--/ home slider -->

        <!-- welcome section -->
        <div class="welcome-section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-6 col-md-6 aos-item" data-aos="fade-down">
                        <img src="img/about01img.jpg" class="img-fluid">
                    </div>
                    <!-- /col -->
                    <!-- col -->
                    <div class="col-lg-6 col-md-6 aos-item align-self-center" data-aos="fade-up">
                        <article class="section-title">
                            <p class="small fgray">About iDream Tech</p>
                            <h1 class="h2">Welcome to iDream Tech</h1>
                        </article>
                        <p>iDream Tech is a subsidiary IT Solutioning company of iDream Media Inc. based out of New
                            Jersey,USA. iDream Tech is an End –to-End solution provider with more than 15 years of
                            collective experience in Management Consulting.</p>

                        <p class="pt-3">
                            <a href="about.php" class="idlink">
                                Read More <span class="icon-arrow-right icomoon"></span>
                            </a>
                        </p>

                        <!-- <img src="img/about02img.jpg" class="img-fluid img-about-second d-none d-lg-block"> -->
                    </div>
                    <!-- /col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ selcome section -->

        <!-- service section -->
        <div class="home-services pt-3">
            <!-- container -->
            <div class="container">
                <article class="section-title">
                    <h3 class="h3">Services We Provide</h3>
                </article>
            </div>
            <!--/ container -->
            <!-- service main -->
            <div class="service-main">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-3 col-sm-6 border-right aos-item" data-aos="fade-down">
                            <div class="service-col">
                                <a href="it-solutions.php"><span class="icon-gear-itsolutions icomoon-col"></span></a>
                                <h4>IT Solutions</h4>
                                <p>We provide application support and maintenance services for enterprises
                                    on windows and linux platforms.</p>
                                <a href="it-solutions.php" class="idlink">
                                    Read More <span class="icon-arrow-right icomoon"></span>
                                </a>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-3 col-sm-6 border-right aos-item" data-aos="fade-up">
                            <div class="service-col">
                                <a href="ites.php"><span class="icon-customer-service icomoon-col"></span></a>
                                <h4>ITES</h4>
                                <p>iDream Tech is a pioneer in helping organizations do their business with ease.
                                </p>
                                <a href="ites.php" class="idlink">
                                    Read More <span class="icon-arrow-right icomoon"></span>
                                </a>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-3 col-sm-6 border-right aos-item" data-aos="fade-down">
                            <div class="service-col">
                                <a href="management-consulting.php"><span class="icon-team icomoon-col"></span></a>
                                <h4>Management Consulting</h4>
                                <p>The need for and advantages of a business process are quite apparent in large
                                    organizations. </p>
                                <a href="management-consulting.php" class="idlink">
                                    Read More <span class="icon-arrow-right icomoon"></span>
                                </a>
                            </div>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-3 col-sm-6 aos-item" data-aos="fade-up">
                            <div class="service-col">
                                <a href="govt-advisory-services.php"><span class="icon-XMLID_1000 icomoon-col"></span></a>
                                <h4>Govt. Advisory Services</h4>
                                <p>We in iDream tech having more than a 150 years of cumulative experience in Advisory and consulting services. Our resources carry expertise from global consulting firms. </p>
                                <a href="govt-advisory-services.php" class="idlink">
                                    Read More <span class="icon-arrow-right icomoon"></span>
                                </a>
                            </div>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->
            </div>
            <!--/ service main -->
        </div>
        <!--/ service section -->

        <!-- our products -->
        <div class="our-products whitebox py20">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -->
                    <div class="col-lg-3">
                        <article class="section-title aos-item" data-aos="fade-up">
                            <p class="small fblue">Our Products</p>
                            <h3 class="h3">work Showcase</h3>
                        </article>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 align-self-center aos-item" data-aos="fade-up">
                        <p>We are committed to providing our customers with exceptional service while offering our
                            employees the best training</p>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                <!-- products row -->
                <div class="row products-row justify-content-center">
                    <!-- col -->
                    <div class="col-lg-4 col-sm-6">
                        <div class="product-col aos-item" data-aos="fade-up">
                            <img src="img/product01.jpg" class="img-fluid" alt="">
                            <a href="cooperative-society-management.php">
                                <article>
                                    <h5>Co-operative Society Management</h5>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority
                                        have suffered alteration in</p>
                                </article>
                            </a>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-4 col-sm-6">
                        <div class="product-col aos-item" data-aos="fade-up">
                            <img src="img/product02.jpg" class="img-fluid" alt="">
                            <a href="integrated-epayservices.php">
                                <article>
                                    <h5>Integratd E-Pay Services</h5>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority
                                        have suffered alteration in</p>
                                </article>
                            </a>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-4 col-sm-6">
                        <div class="product-col aos-item" data-aos="fade-up">
                            <img src="img/product03.jpg" class="img-fluid" alt="">
                            <a href="smart-virtual-classes.php">
                                <article>
                                    <h5>Smart &amp; Virtual Classes</h5>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority
                                        have suffered alteration in</p>
                                </article>
                            </a>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-4 col-sm-6">
                        <div class="product-col aos-item" data-aos="fade-up">
                            <img src="img/product04.jpg" class="img-fluid" alt="">
                            <a href="iot-services.php">
                                <article>
                                    <h5>IoT Services</h5>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority
                                        have suffered alteration in</p>
                                </article>
                            </a>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-4 col-sm-6">
                        <div class="product-col aos-item" data-aos="fade-up">
                            <img src="img/product05.jpg" class="img-fluid" alt="">
                            <a href="hrms-solutions.php">
                                <article>
                                    <h5>HRMS Solutions</h5>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority
                                        have suffered alteration in</p>
                                </article>
                            </a>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-4 col-sm-6">
                        <div class="product-col aos-item" data-aos="fade-up">
                            <img src="img/product06.jpg" class="img-fluid" alt="">
                            <a href="school-management.php">
                                <article>
                                    <h5>School Management</h5>
                                    <p>There are many variations of passages of Lorem Ipsum available, but the majority
                                        have suffered alteration in</p>
                                </article>
                            </a>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ products row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ our products -->

        <!-- insights -->
        <div class="container py20">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6 col-sm-6 align-self-center aos-item" data-aos="fade-up">
                    <article class="section-title">
                        <h1 class="h2 pb-3">Insights and Resources to help drive your Business Forward Faster.</h1>
                        <p class="pb-3 text-justify">The IT and ITeS sector in India stood at US$177 billion in 2019 witnessing a growth of 6.1 per cent year-on-year and is estimated that the size of the industry will grow to US$ 350 billion by 2025. India’s IT & ITeS industry grew to US$ 181 billion in 2018-19. Exports from the industry increased to US$ 137 billion in FY19 while domestic revenues (including hardware) advanced to US$ 44 billion. IT industry employees 4.1 million people as of FY19.</p>

                        <p class="text-justify">Revenue from digital segment is expected to comprise 38 per cent of the forecasted US$ 350 billion industry revenue by 2025.</p>
                        
                    </article>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-6 col-sm-6 align-self-center aos-item" data-aos="fade-down">
                    <img src="img/business-img.png" alt="" class="img-fluid">
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div>
        <!--/ insights -->

        <!-- partners -->
        <div class="partners">
            <!-- container -->
            <div class="container">
                <article class="section-title aos-item" data-aos="fade-up">
                    <h3 class="h3 text-center">Our Partners</h3>
                </article>
            </div>
            <!-- container -->
            <div class="partners-logos d-flex justify-content-center aos-item" data-aos="fade-down">
                <img src="img/partner01.jpg" alt="">
                <img src="img/partner02.jpg" alt="">
                <img src="img/partner03.jpg" alt="">
                <img src="img/partner04.jpg" alt="">
                <img src="img/partner05.jpg" alt="">
                <img src="img/partner06.jpg" alt="">
                <img src="img/partner07.jpg" alt="">
                <img src="img/partner08.jpg" alt="">
            </div>

        </div>
        <!--/ partners -->

    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>