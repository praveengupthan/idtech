<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iDream Tech Contact</title>
    <link rel="icon" type="image/png" sizes="32x32" href="img/android-icon-36x36.png">
    <?php include 'styles.php' ?>
</head>

<body>
    <?php include 'header.php' ?>
    <!-- main -->
    <main>

    <!-- sub page -->
    <div class="sub-page">
        <!-- sub page header -->
        <div class="subpage-header">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- col -left -->
                    <div class="col-lg-6">
                        <h1>Contact</h1>                       
                    </div>
                    <!--/ col left -->
                    <!-- col -right -->
                    <div class="col-lg-6 text-lg-right align-self-md-center">
                        <ul class="brcrumb">
                            <li><a href="index.php">Home</a></li>
                            <li class="active"><a href="index.php">Contact</a></li>
                        </ul>
                    </div>
                    <!--/ col right -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subpage-body">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- left col -->
                    <div class="col-lg-6 aos-item" data-aos="fade-up">
                        <h3 class="h5 fbold">Where we are</h2>
                        <!-- address -->
                        <div class="address">
                            <h3 class="h5 fblue fbold">Hyderabad, India</h3>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td><span class="icon-location-arrow icomoon"></span></td>
                                        <td>25, 8-3-833/25, Aurora Colony Rd, Sagar Society, Sri Nagar Colony, Aurora Colony, Banjara Hills, Hyderabad, Telangana 500073</td>
                                    </tr>
                                    <tr>
                                        <td><span class="icon-envelope icomoon"></span></td>
                                        <td>finance@idreammedia.com, ceo@idreamtech.net</td>
                                    </tr>
                                    <tr>
                                        <td><span class="icon-phone-square icomoon"></span></td>
                                        <td>+91 40-23740503, +91 9642437773</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--/ address-->

                        <!-- address -->
                        <div class="address aos-item" data-aos="fade-down">
                            <h3 class="h5 fblue fbold">Bhubaneshwar, India</h3>
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <td><span class="icon-location-arrow icomoon"></span></td>
                                        <td>Plot no 1464 new Siripur Sahi,  behind city women college Bhubaneswar-03,  Dist:khurdha,  Odisha.</td>
                                    </tr>
                                   
                                    <tr>
                                        <td><span class="icon-phone-square icomoon"></span></td>
                                        <td>+91 7682046036</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!--/ address-->  
                    </div>
                    <!--/ left col -->

                    <!-- right col -->
                    <div class="col-lg-6 aos-item" data-aos="fade-down">
                        <h3 class="h5 fbold">Reach us</h2>

                        <?php 
                            if(isset($_POST['submit'])){
                                
                            $to = "info@idreamtech.net"; 
                            $subject = "Mail From ".$_POST['name'];
                            
                            $message = "
                            <html>
                            <head>
                            <title>HTML email</title>
                            </head>
                            <body>
                            <p>".$_POST['name']." has sent mail!</p>
                                <table>
                                    <tr>
                                        <th align='left'>Name</th>
                                        <td>".$_POST['name']."</td>
                                    </tr>
                                   
                                    <tr>
                                        <th align='left'>Email</th>
                                        <td>".$_POST['email']."</td>
                                    </tr>
                                    <tr>
                                        <th align='left'>Phone</th>
                                        <td>".$_POST['phone']."</td>
                                    </tr>
                                    <tr>
                                        <th align='left'>Subject</th>
                                        <td>".$_POST['subject']."</td>
                                    </tr>
                                    <tr>
                                        <th align='left'>Message</th>
                                        <td>".$_POST['msg']."</td>
                                    </tr>
                                </table>
                            </body>
                            </html>
                            ";

                            // Always set content-type when sending HTML email
                            $headers = "MIME-Version: 1.0" . "\r\n";
                            $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
                            // More headers
                            $headers .= 'From:' .$_POST['name']. "\r\n";
                            mail($to,$subject,$message,$headers);                            
                            echo "<p style='color:green'> Mail Sent. Thank you " . $_POST['name'] .", we will contact you shortly. </p>";
                            }
                        ?>

                        <!-- form -->
                        <form class="form" method="post">
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Name <span class="fred">*</span></label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Write Your Name" name="name" required>
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->                               

                                 <!-- col -->
                                 <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Mobile Number <span class="fred">*</span></label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Mobile Number" name="phone" required>
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Email <span class="fred">*</span></label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Email" name="email" required>
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                  <!-- col -->
                                  <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>Subject</label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" placeholder="Write Subject" name="subject" required >
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>Message</label>
                                        <div class="input-group">
                                           <textarea class="form-control" style="height:120px;" placeholder="Write your Message" name="msg"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-12">                                  

                                    <input type="submit" value="Submit" class="btn cust-btn" name="submit">

                                    
                                </div>
                                <!--/ col -->

                            </div>
                            <!--/ row -->
                        </form>
                        <!--/ form -->
                    </div>
                    <!--/ right col -->
                </div>
                <!-- /row -->               
            </div>
            <!--/ container -->    
            
            <!-- container fluid -->
            <div class="container-fluid mt-3 aos-item" data-aos="fade-up">
                 <!-- row -->
                <div class="row">
                    <div class="col-lg-12">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3806.6457270712544!2d78.4342283!3d17.4287813!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb90d468d4ba0f%3A0x3797df2e8df2a81b!2siDream%20Media!5e0!3m2!1sen!2sin!4v1582696585834!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                </div>
                <!-- row -->
            </div>
            <!--/container fluid -->
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ sub apge ends -->

    </main>
    <!--/ main -->
    <?php include 'footer.php' ?>

    <?php include 'scripts.php' ?>
</body>

</html>